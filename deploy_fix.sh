#!/bin/bash

mix edeliver build release
mix edeliver deploy release to staging

echo "FIXING vm.args DEPLOYMENT"
declare -a ip_addresses=( "10.211.55.5" "10.211.55.6" )
for ip in "${ip_addresses[@]}"
do
    scp -3 -i ~/.ssh/id_rsa root@$ip:/opt/vm.args root@$ip:/opt/octopus_project/octopus_project/releases/0.1.0/vm.args
    echo "FIXED vm.args ON $ip node"
done
