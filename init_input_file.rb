include Math

def f1(x)
  0.0
end

def f2(x)
  2.0
end

def f3(x)
  (2.0 - x) * (2.0 - x) / 2.0
end

def f4(x)
  2.0 - x
end

def func(x, y)
  (exp(-x) + exp(-y)) / 10.0
end

SIZE = ARGV[0]&.to_i || 20
FILENAME = 'input_file.in'
dx = 1.0 / SIZE
dy = 1.0 / SIZE

res = Array.new(SIZE, [])
res.map! do |r|
  r = Array.new(SIZE, 0.0)
end

for j in 0...SIZE
  res[0][j] = f1(j * dx)
end
for j in 0...SIZE
  res[SIZE-1][j] = f2(j * dx)
end
for i in 0...SIZE
  res[i][0] = f3((SIZE - i) * dy)
end
for i in 0...SIZE
  res[i][SIZE - 1] = f4((SIZE - i) * dy)
end

File.open(FILENAME, 'w') do |f|
  res.each do |row|
    f.write(row.join("\t").concat("\n"))
  end
end
