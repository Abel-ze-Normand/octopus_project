defmodule OctopusProject.DStorage do
  @moduledoc """
  Distributed storage manager. Uses mnesia as backend to store data,
  schema for data storage located in lib/database.ex
  """
  # TODO 11.11.2016 not sure, probably change abstractions from <call> to <cast>
  # for better performace and async

  require Logger
  use GenServer
  require Amnesia
  use Amnesia
  use Database
  use Storage

  ##############
  # Public Api #
  ##############

  @doc """
  Start distributed storage manager
  """
  def start_link(args \\ []) do
    Logger.info "Starting mnesia manager..."
    pid = GenServer.start_link(__MODULE__, args, name: __MODULE__)
    Logger.info "Mnesia manager successfully started"
    pid
  end

  @doc """
  Stops distributed storage manager
  """
  def stop do
    Logger.info "Stopping mnesia manager..."
    GenServer.stop(__MODULE__)
    Logger.info "Mnesia manager successfully stopped"
  end

  @doc """
  Write data in storage by given key and tag. Existing data by this key (with
  optional tag) will be overwritten
  Note: it’s especially advised for using as tag static atoms, not dynamic, keep in
  mind that atoms can’t be garbage collected
  """
  @spec store(String.t, any, String.t | Atom.t) :: :ok
  def store(key, value, tag \\ :unknown) do
    :ok = GenServer.call(__MODULE__, {:store, key, value, tag})
  end

  @doc """
  Write list of records to storage. List of records must be formatted as tuple like
  {key, value, tag}. Existing data by this keys (with optional tags) will be overwritten

  Note: it’s especially advised for using as tag static atoms, not dynamic, keep in
  mind that atoms can’t be garbage collected
  """
  @spec bulk_store(List.t) :: :ok
  def bulk_store(list) when is_list(list) do
    GenServer.call(__MODULE__, {:bulk_store, list})
  end

  def bulk_store(_), do: {:error, :enolist}

  @doc """
  Find records with matching keys and given tag. Does not remove records from storage
  """
  @spec bulk_retrieve(List.t, Atom.t) :: {:ok, List.t} | {:error, :enoentries}
  def bulk_retrieve(keys, tag \\ :unknown) do
    GenServer.call(__MODULE__, {:bulk_retrieve, keys, tag})
  end

  @doc """
  Find records by given tag and remove found records from storage
  """
  @spec bulk_retrieve_and_forget(String.t | Atom.t, Integer.t | Atom.t) :: {:ok, any}
  def bulk_retrieve_and_forget(tag, max_count \\ :all) do
    GenServer.call(__MODULE__, {:bulk_retrieve_and_forget, tag, max_count})
  end

  @doc """
  Retrieve data by given key and/or tag. Does not remove record by given key+tag from storage
  """
  @spec retrieve(String.t, String.t | Atom.t) :: {:ok, any} | {:error, :eunknownkey} | {:error, :eformaterr}
  def retrieve(key, tag \\ :unknown) do
    GenServer.call(__MODULE__, {:retrieve, key, tag})
  end

  @doc """
  Removes record by given key (with optional tag) from database
  """
  @spec forget(String.t, String.t | Atom.t) :: :ok
  def forget(key, tag \\ :unknown) do
    GenServer.call(__MODULE__, {:forget, key, tag})
  end

  @doc """
  Select data matching by given tag
  """
  @spec lookup(String.t | Atom.t) :: {:ok, any} | {:error, :enotag}
  def lookup(tag) do
    GenServer.call(__MODULE__, {:lookup, tag})
  end

  @doc """
  Find record by given key and tag. If record exists, record
  will be returned and removed from storage
  """
  @spec retrieve_and_forget(String.t, String.t | Atom.t) :: {:ok, any} | {:error, :eunknownkey} | {:error, :eformaterr}
  def retrieve_and_forget(key, tag \\ :unknown) do
    GenServer.call(__MODULE__, {:retrieve_and_forget, key, tag})
  end

  #######################
  # GenServer callbacks #
  #######################

  def init(_args) do
    try do
      Logger.info "Creating mnesia database schema..."
      Amnesia.Schema.create
      Logger.info "Mnesia database schema successfully created"
    rescue
      _ -> Logger.warn "Database schema already exists! Fallback to existing schema."
    end
    Amnesia.start
    # TODO change ’[self]' to compiled list of nodes
    Database.create(ram_copies: [self()], diskonly_copies: [])
    Database.wait
    Logger.info "Mnesia database successfully initialized"
    {:ok, []}
  end

  def handle_call({:store, key, value, tag}, _from, _state) do
    handle_store(key, value, tag)
    {:reply, :ok, []}
  end

  def handle_call({:bulk_store, list}, _from, _state) do
    handle_bulk_store(list)
    {:reply, :ok, []}
  end

  def handle_call({:retrieve, key, tag}, _from, _state) do
    res = handle_retrieve(key, tag)
    {:reply, res, []}
  end

  def handle_call({:retrieve_and_forget, key, tag}, _from, _state) do
    res = handle_retrieve_and_forget(key, tag)
    {:reply, res, []}
  end

  def handle_call({:forget, key, tag}, _from, _state) do
    handle_forget(key, tag)
    {:reply, :ok, []}
  end

  def handle_call({:lookup, search_tag}, _from, _state) do
    res = handle_lookup(search_tag)
    {:reply, res, []}
  end

  def handle_call({:bulk_retrieve, keys, tag}, _from, _state) do
    res = handle_bulk_retrieve(keys, tag)
    {:reply, res, []}
  end

  def handle_call({:bulk_retrieve_and_forget, search_tag, max_count}, _from, _state) do
    res = handle_bulk_retrieve_and_forget(search_tag, max_count)
    {:reply, res, []}
  end

  def terminate(:normal, _state), do: gracefully_close()
  def terminate(:shutdown, _state), do: gracefully_close()
  def terminate(reason, state), do: super(reason, state)

  #####################
  # Private functions #
  #####################

  defp gracefully_close do
    Logger.info "Gracefully closing database..."
    Database.destroy
    Amnesia.stop
    Amnesia.Schema.destroy
    Logger.info "Gracefully closed database"
  end

  defp to_take(stream, :all), do: stream
  defp to_take(stream, num) when is_integer(num), do: stream |> Stream.take(num)
  defp to_take(_stream, _num), do: :error

  defp handle_store(key, value, tag) do
    Amnesia.transaction do
      %Storage{key: {key, tag}, tag: tag, payload: value}
      |> Storage.write
    end
  end

  defp handle_bulk_store(list) do
    Amnesia.transaction do
      Enum.each list, fn (t) ->
        r = case t do
              {key, value} -> %Storage{key: {key, :unknown}, tag: :unknown, payload: value}
              {key, value, tag} -> %Storage{key: {key, tag}, tag: tag, payload: value}
            end
        r |> Storage.write
      end
    end
  end

  defp handle_bulk_retrieve(keys, target_tag) do
    transaction_res = Amnesia.transaction do
      for k <- keys do
        [v] = Storage.where(key == {k, target_tag}, select: [key, payload, tag]).values
        v
      end
    end
    r = Enum.map transaction_res, fn([{key, tag}, value, _tag]) -> {key, value, tag} end
    if transaction_res |> Enum.empty? do
      {:error, :enoentries}
    else
      {:ok, r}
    end
  end

  defp handle_bulk_retrieve_and_forget(search_tag, max_count) do
    transaction_res = Amnesia.transaction do
      filt = Storage.where tag == search_tag, select: [key, payload, tag]
      filt |> Amnesia.Selection.values |> to_take(max_count)
    end
    r = Enum.map transaction_res, fn([{key, tag}, value, _tag]) -> {key, value, tag} end
    res = case r do
            [] -> {:error, :enotag}
            _ -> {:ok, r}
          end
    Amnesia.transaction do
      for {key, _value, tag} <- r do
        Storage.delete({key, tag})
      end
    end
    res
  end

  defp handle_retrieve_and_forget(key, tag) do
    case handle_retrieve(key, tag) do
      {:ok, payload} ->
        handle_forget(key, tag)
        {:ok, payload}
      x ->
        x
    end
  end

  defp handle_lookup(search_tag) do
    transaction_res = Amnesia.transaction do
      filt = Storage.where tag == search_tag, select: [key, payload, tag]
      filt |> Amnesia.Selection.values
    end
    r = Enum.map transaction_res, fn([{key, tag}, value, _tag]) -> {key, value, tag} end
    case r do
      [] -> {:error, :enotag}
      _ -> {:ok, r}
    end
  end

  defp handle_retrieve(key, tag) do
    transaction_res = Amnesia.transaction do
      Storage.read({key, tag})
    end
    case transaction_res do
      %Storage{key: {_key, _tag}, payload: payload, tag: _} ->
        {:ok, payload}
      nil ->
        {:error, :eunknownkey}
      _ ->
        {:error, :eformaterr}
    end
  end

  defp handle_forget(key, tag) do
    Amnesia.transaction do
      Storage.delete({key, tag})
    end
  end
end
