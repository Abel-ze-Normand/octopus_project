defmodule OctopusProject.Groupers.NoOverlapGrouper do
  @moduledoc """
  Groups elements of given range where successor prefix overlaps with predessor suffix.
  Splits equally (tries, at least) for each bucket, but if there is remainder at the tail -
  remainder will pushed to last elem

  Example: given [1, 2, 3, 4, 5, 6, 7] and group by overlapping 1 with bucket size 3:
  result will be [[1, 2, 3], [3, 4, 5], [5, 6, 7]]

  Example: given [1, 2, 3, 4, 5, 6, 7] and group by overlapping 1 with bucket size 4:
  result will be [[1, 2, 3, 4], [4, 5, 6, 7]]
  """
  @type t :: %OctopusProject.Groupers.NoOverlapGrouper{
    range: Range.t,
    workers_count: integer,
  }
  defstruct range: 0..1, workers_count: 0
end

defimpl OctopusProject.Grouper, for: OctopusProject.Groupers.NoOverlapGrouper do
  def group(grouper) do
    per_bucket = div(Enum.count(grouper.range), grouper.workers_count)
    Enum.chunk(grouper.range, per_bucket, per_bucket, [])
  end
end
