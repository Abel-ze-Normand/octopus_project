defmodule OctopusProject.Groupers.OverlapGrouper do
  @moduledoc """
  Groups elements of given range where successor prefix overlaps with predessor suffix.
  Splits equally (tries, at least) for each bucket, but if there is remainder at the tail -
  remainder will pushed to last elem

  Example: given [1, 2, 3, 4, 5, 6, 7] and group by overlapping 1 with bucket size 3:
  result will be [[1, 2, 3], [3, 4, 5], [5, 6, 7]]

  Example: given [1, 2, 3, 4, 5, 6, 7] and group by overlapping 1 with bucket size 4:
  result will be [[1, 2, 3, 4], [4, 5, 6, 7]]
  """
  @type t :: %OctopusProject.Groupers.OverlapGrouper{
    range: Range.t,
    workers_count: integer,
    per_bucket: integer,
    overlap_amount: integer,
    strategy: :fair | :constant
  }
  defstruct range: 0..1, workers_count: 0, per_bucket: 1, overlap_amount: 1, strategy: :fair
end

defimpl OctopusProject.Grouper, for: OctopusProject.Groupers.OverlapGrouper do
  def group(grouper = %OctopusProject.Groupers.OverlapGrouper{strategy: :fair}) do
    range_list = grouper.range |> Enum.into([])
    per_worker = length(range_list) / grouper.workers_count
    group_keys(range_list, per_worker, grouper.overlap_amount)
  end

  def group(grouper = %OctopusProject.Groupers.OverlapGrouper{strategy: :constant}) do
    group_keys(grouper.range |> Enum.into([]), grouper.per_bucket, grouper.overlap_amount)
  end

  ########

  defp group_keys(keys, chunk_size, overlap_size) when overlap_size < chunk_size,
    do: Enum.chunk(keys, round(chunk_size), round(chunk_size - overlap_size), [])
end
