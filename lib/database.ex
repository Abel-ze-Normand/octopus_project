require Amnesia
use Amnesia

defdatabase Database do
  deftable Storage, [:key, :tag, :payload], type: :set do
    @type t :: %Storage{key: {String.t, String.t | Atom.t}, tag: String.t | Atom.t, payload: any}
  end
end
