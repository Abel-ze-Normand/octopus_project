defprotocol OctopusProject.Grouper do
  @doc "Groups range of keys by given rule"
  def group(range)
end
