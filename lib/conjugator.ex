defmodule OctopusProject.Conjugator do
  @moduledoc """
  Module for generating tasks conjugation rules and its execution

  *Conjugation* - is a process of comparing shared data in two data buckets,
  and execution of *merging* this buckets if *exit-merge-reason* is satisfied.
  If *exit-merge-reason* is not satisfied then this buckets will be *mutated* by some rule
  and new conjugation tasks will be generated.

  Let’s explain each step

  # Merging
  High-level operation when two record buckets will coerced by specified rule to produce one
  bigger bucket. Must return new bucket and new conjugation task.
  N.B. two dependent buckets will create new conjugation task, that will contain self - newly produced ’big’
  bucket - and it’s future neighbour. So it’s neighour must produce absolutely EXACT copy of this conjugation task.
  With this rule there is no difference whom first from 2 future neighbours will create future conjugation task.
  ## Example:
  [1, 2], [2, 3], [3, 4], [4, 5]

  first two bucket and last two buckets will be merged each by it’s pair:
  [1, 2, 3], [3, 4, 5]

  so first pair [1, 2], [2, 3] will create conjugation task:
  [[1, 2, 3], [3, 4, 5]]

  and last pair _must_ generate same conjugation task

  # Exit-merge-reason
  Function that returns :ok or :not_enough based on some compare rule between given two buckets

  # Mutation
  Rule to modification two given buckets for continuation of their computations.
  Takes as parameter two buckets and return two mutated buckets

  # Conjugation
  With described 3 functions conjugator will produce it’s decision of /partial equality/ of shared data
  between two buckets. If *exit-merge-reason* satisfied will return
  %{status: :merged, buckets: [<new merged to one bucket data>], conj_task: <conj_task>}
  else
  %{status: :not_merged, buckets: [<mutated bucket 1>, <mutated bucket 2>], conj_task: <conj_task>}

  For conjugation order is responsible *conjugation tree*, that must be built separately from conjugator.
  Conjugator will further reduce tree from leaves to root, when reduction is appliable

  Conjugation binary tree structure:
  non-leaves = left: Tree, right: Tree, val: nil
  leaves = left: nil, right: nil, val: <bucket_name>
  """

  @type bucket_name :: String.t
  @type key_name :: String.t
  @type conj_tree :: OctopusProject.Tree.t
  @type conj_task :: {:conj_task, bucket_name, bucket_name} | {:final, {:conj_task, bucket_name, bucket_name}} | nil
  @type record :: {key_name, any}
  @type bucket :: {:bucket, {bucket_name, [record]}}

  @callback merge(
    bucket1 :: bucket,
    bucket2 :: bucket,
    merged_name :: String.t,
    shared_keys :: [key_name])
  :: bucket
  @callback exit_merge_reason?(
    bucket1 :: bucket,
    bucket2 :: bucket,
    shared_keys :: [key_name]
  ) :: boolean
  @callback mutate_buckets(
    bucket1 :: bucket,
    bucket2 :: bucket,
    shared_keys :: [key_name]
  ) :: {{:bucket_1, bucket}, {:bucket_2, bucket}}


  defmacro __using__(_params) do
    quote do
      @behaviour OctopusProject.Conjugator
      alias OctopusProject.{Conjugator, Tree}
      require Logger

      @type bucket_name :: String.t
      @type key_name :: String.t
      @type conj_tree :: OctopusProject.Tree.t
      @type conj_task :: {:conj_task, bucket_name, bucket_name} | :final
      @type record :: {key_name, any}
      @type bucket :: {:bucket, {bucket_name, [record]}}

      @type conjugation_decision :: %{
        status: :merged | :unmerged,
        buckets: [any],
        conj_task: conj_task,
        conj_tree: conj_tree
      }

      @type conj_fun_task_in :: %{conj_task_data: {bucket, bucket}, conj_tree: OctopusProject.Tree.t}

      @spec conjugate_fun(conj_fun_task_in, pid, [String.t], String.t | Atom.t) :: any
      def conjugate_fun(task_data, master_pid, bucket_names, _executable_name) do
        %{conj_task_data: {b1, b2}, conj_tree: conj_tree} = task_data
        {:bucket, {b1k, _}} = b1
        {:bucket, {b2k, _}} = b2
        sorted_b1 = natural_sort_bucket_keys(b1)
        sorted_b2 = natural_sort_bucket_keys(b2)
        res = conjugate(b1, b2, conj_tree)
        send(master_pid, {:conj_done, res, [b1k, b2k]})
      end

      @spec natural_sort_bucket_keys(bucket) :: bucket
      def natural_sort_bucket_keys({:bucket, {b_k, lines}}) do
        new_lines = Enum.sort_by(lines, fn {k, _} ->
          Regex.named_captures(~r/[A-Za-z_]+(?<num>[\d]+)/, k)["num"] |> Integer.parse() |> elem(0)
        end)
        {:bucket, {b_k, new_lines}}
      end

      @spec conjugate(bucket_1 :: bucket, bucket_2 :: bucket, conj_tree :: conj_tree) :: conjugation_decision
      def conjugate(bucket_1, bucket_2, conj_tree) do
        shared_data_keys = get_shared_data_keys(bucket_1, bucket_2)
        if exit_merge_reason?(bucket_1, bucket_2, shared_data_keys) do
          merged_bucket_name = gen_merged_bucket_name(bucket_1, bucket_2)
          new_bucket = merge(bucket_1, bucket_2, merged_bucket_name, shared_data_keys)

          next_conj_res = get_conj_task(:merged, bucket_1, new_bucket, conj_tree)

          %{
            status: :merged,
            buckets: [new_bucket],
            conj_task: next_conj_res[:conj_task],
            conj_tree: next_conj_res[:conj_tree]
          }
        else
          {{:bucket_1, mut_bucket_1}, {:bucket_2, mut_bucket_2}} = mutate_buckets(bucket_1, bucket_2, shared_data_keys)

          %{
            status: :unmerged,
            buckets: [mut_bucket_1, mut_bucket_2],
            conj_task: get_conj_task(:unmerged, bucket_1, bucket_2),
            conj_tree: conj_tree
          }
        end
      end

      @spec gen_merged_bucket_name(bucket_1 :: bucket | String.t | nil, bucket_2 :: bucket | String.t | nil) :: String.t
      defp gen_merged_bucket_name(nil, nil) do
        "[]"
      end

      defp gen_merged_bucket_name(bucket_1, nil) do
        "[#{bucket_1}]"
      end

      defp gen_merged_bucket_name(nil, bucket_2) do
        "[#{bucket_2}]"
      end

      defp gen_merged_bucket_name(bucket_1_name, bucket_2_name) when is_binary(bucket_1_name) and is_binary(bucket_2_name) do
        "[" <> bucket_1_name <> "+" <> bucket_2_name <> "]"
      end

      defp gen_merged_bucket_name(bucket_1, bucket_2) when not (is_binary(bucket_1) and is_binary(bucket_2)) do
        {:bucket, {name_1, _}} = bucket_1
        {:bucket, {name_2, _}} = bucket_2
        "[" <> name_1 <> "+" <> name_2 <> "]"
      end

      @spec get_shared_data_keys(bucket_1 :: bucket, bucket_2 :: bucket) :: [key_name]
      defp get_shared_data_keys(bucket_1, bucket_2) do
        {:bucket, {_, lines_1}} = bucket_1
        {:bucket, {_, lines_2}} = bucket_2
        keys_1 = lines_1 |> Enum.map(fn {k, v} -> k end)
        keys_2 = lines_2 |> Enum.map(fn {k, v} -> k end)
        keys_1 -- (keys_1 -- keys_2)
      end


      # Returns new conjugation task
      #
      # Note: Because conjugations graph is acyclic so there is no need in second bucket
      @spec get_conj_task(:merged, leaf_bucket :: bucket, merged_bucket :: bucket, conj_tree :: Tree.t) ::
      [conj_task: conj_task, conj_tree: Tree.t]
      defp get_conj_task(:merged, leaf_bucket, merged_bucket, conj_tree) do
        {:bucket, {merged_bucket_name, _}} = merged_bucket
        {:bucket, {leaf_bucket_name, _}} = leaf_bucket

        bucket_1_and_2_subtree = Tree.find_parent_by_v(conj_tree, leaf_bucket_name)
        reduced_buckets_1_2_subtree = Tree.new(nil, merged_bucket_name, nil)

        # if final => nil
        future_conj_tree = Tree.find_parent(conj_tree, bucket_1_and_2_subtree)

        sibling_subtree = get_sibling_subtree(future_conj_tree, bucket_1_and_2_subtree)

        if sibling_subtree do
          updated_left_right_future_node =
          if future_conj_tree.left == bucket_1_and_2_subtree do
            {reduced_buckets_1_2_subtree, nil, sibling_subtree}
          else
            {sibling_subtree, nil, reduced_buckets_1_2_subtree}
          end

          new_conj_tree = Tree.update_node(conj_tree, future_conj_tree, updated_left_right_future_node)

          sibling_name = cond do
            is_nil(sibling_subtree) -> nil
            sibling_subtree.value -> sibling_subtree.value
            true -> gen_merged_bucket_name(sibling_subtree.left.value, sibling_subtree.right.value)
          end

          next_conj_task = if Tree.root?(conj_tree, future_conj_tree) do
            {
              :final,
              {
                :conj_task,
                merged_bucket_name,
                sibling_name
              }
            }
          else
            {
              :conj_task,
              merged_bucket_name,
              sibling_name
            }
          end
          [
            conj_task: next_conj_task,
            conj_tree: new_conj_tree
          ]
        else
          [
            conj_task: nil,
            conj_tree: (if is_nil(future_conj_tree), do: reduced_buckets_1_2_subtree, else: conj_tree)
          ]
        end
      end

      defp get_sibling_subtree(nil, _) do
        nil
      end

      defp get_sibling_subtree(future_conj_tree, bucket_1_and_2_subtree) do
        if future_conj_tree.left == bucket_1_and_2_subtree do
          future_conj_tree.right
        else
          future_conj_tree.left
        end
      end

      @spec get_conj_task(:unmerged, bucket_1 :: bucket, bucket_2 :: bucket) :: conj_task
      defp get_conj_task(:unmerged, bucket_1, bucket_2) do
        {:bucket, {name_1, _}} = bucket_1
        {:bucket, {name_2, _}} = bucket_2
        {:conj_task, name_1, name_2}
      end
    end
  end
end
