defmodule OctopusProject.Tree do
  alias OctopusProject.Tree
  @type subtree :: Tree.t | nil
  @type value_t :: term | nil
  @type t :: %Tree{left: subtree, right: subtree, value: value_t, empty_value: boolean}
  defstruct left: nil, right: nil, value: nil, empty_value: false

  @moduledoc """
  Simple immutable binary tree abstraction
  """

  @doc """
  Tree constructor
  """
  @spec new(left :: subtree, value :: value_t, right :: subtree, empty :: boolean) :: Tree.t
  def new(left, value, right, empty \\ false) do
    %Tree{left: left, value: value, right: right, empty_value: empty}
  end

  @doc """
  Generate ideally balanced tree with given keys as leaves.
  If keys list size is not power of 2, placeholder will fill rest of leaves values
  """
  @spec reverse_grow_tree(leaves :: [any], placeholder :: any) :: Tree.t
  def reverse_grow_tree(leaves, placeholder \\ "EMPTY") do
    # more optimal to grow tree from leaves to root in case of structs immutability
    leaves_cnt = leaves |> Enum.count()
    depth = leaves_cnt |> :math.log2() |> Float.ceil() |> round()
    ideal_leaves_cnt = :math.pow(2, depth) |> round

    to_add_cnt = ideal_leaves_cnt - leaves_cnt
    to_add = for _ <- 1..to_add_cnt, do: placeholder

    leaves_actual = leaves ++ to_add

    # generate initial subtrees (elementary subtrees with only value)
    start = leaves_actual |> Enum.map(&(Tree.new(nil, &1, nil, &1 == placeholder)))

    do_rev_grow_tree(start)
  end

  defp do_rev_grow_tree([t]) do
    t
  end

  defp do_rev_grow_tree(subtrees) do
    subtrees_spliced = subtrees
    |> Enum.chunk(2)
    |> Enum.map(fn [l, r] ->
      cond do
        l.empty_value and not r.empty_value -> r
        not l.empty_value and r.empty_value -> l
        l.empty_value and r.empty_value     -> Tree.new(nil, nil, nil, true)
        true                                -> Tree.new(l, nil, r)
      end
    end)
    do_rev_grow_tree(subtrees_spliced)
  end

  @doc """
  Find subtree with given value
  """
  @spec find(tree :: subtree, value :: any) :: subtree
  def find(tree, value) when not is_nil(tree) do
    if tree.value == value do
      tree
    else
      find(tree.left, value) || find(tree.right, value)
    end
  end

  def find(nil, _) do
    nil
  end

  @doc """
  Find parent subtree for given subtree
  """
  @spec find_parent(tree :: subtree, subtree :: Tree.t) :: subtree
  def find_parent(tree, subtree) when not is_nil(tree) do
    if tree.left == subtree or tree.right == subtree do
      tree
    else
      find_parent(tree.left, subtree) || find_parent(tree.right, subtree)
    end
  end

  def find_parent(nil, _) do
    nil
  end

  @doc """
  Find parent subtree for subtree with given value. If values in tree repeated, behaviour is undefined
  """
  @spec find_parent_by_v(tree :: subtree, value :: value_t) :: subtree
  def find_parent_by_v(tree, value) when not is_nil(tree) do
    if (tree.left && tree.left.value == value) || (tree.right && tree.right.value == value) do
      tree
    else
      find_parent_by_v(tree.left, value) || find_parent_by_v(tree.right, value)
    end
  end

  def find_parent_by_v(nil, _) do
    nil
  end

  @doc """
  Reconstruct tree with given subtree to update
  """
  @spec update_node(tree :: Tree.t | nil, subtree :: subtree, {subtree, value_t, subtree}) :: Tree.t | nil
  def update_node(tree, subtree, to_upd_tpl = {left, v, right}) when not is_nil(tree) do
    if tree == subtree do
      Tree.new(left, v, right)
    else
      Tree.new(
        update_node(tree.left, subtree, to_upd_tpl),
        tree.value,
        update_node(tree.right, subtree, to_upd_tpl)
      )
    end
  end

  def update_node(nil, _, _) do
    nil
  end

  @doc """
  Check given subtree is tree’s root
  """
  @spec root?(tree :: Tree.t, subtree :: subtree) :: boolean
  def root?(tree, subtree) do
    tree == subtree
  end

  @doc """
  Check for given values that associated subtrees have shared parent. If values in tree repeated, behaviour is undefined
  """
  @spec same_parent_by_v?(tree :: Tree.t, lv :: any, rv :: any) :: boolean
  def same_parent_by_v?(tree, lv, rv) when not is_nil(tree) do
    if this_subtree?(tree, lv, rv) do
      true
    else
      same_parent_by_v?(tree.left, lv, rv) || same_parent_by_v?(tree.right, lv, rv)
    end
  end

  def same_parent_by_v?(nil, _, _) do
    false
  end

  defp this_subtree?(t, lv, rv) do
    (
      (t.left && t.left.value == lv) && (t.right && t.right.value == rv)
    ) || (
      (t.right && t.right.value == lv) && (t.left && t.left.value == rv)
    )
  end
end
