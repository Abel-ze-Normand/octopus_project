defmodule OctopusProject do
  use Application
  require Logger
  import Supervisor.Spec, warn: false
  alias OctopusProject.DStorage


  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: OctopusProject.Supervisor]
    Logger.info "Starting components and supervisor"
    Application.get_env(:octopus_project, :environ)
    |> supervision_collection()
    |> Supervisor.start_link(opts)
  end

  # TODO extract code below to <<Configurator>>
  # better expand them in macros through "use" macros
  def supervision_collection(:test), do: []
  def supervision_collection(env) when env in [:dev, :prod] do
          [
        # Starts a worker by calling: OctopusProject.Worker.start_link(arg1, arg2, arg3)
        # worker(OctopusProject.Worker, [arg1, arg2, arg3]),
        supervisor(OctopusProject.Broker.WorkersSupervisor, broker_workers_sup_args(), restart: :transient),
        worker(OctopusProject.DStorage, d_storage_args(), restart: :transient),
        worker(OctopusProject.Broker, broker_args(), restart: :transient),
        worker(OctopusProject.Queue, queue_args(), restart: :transient),
        worker(OctopusProject.TaskGenerator, task_gen_args(), restart: :transient),
        worker(OctopusProject.InputFacility, ifac_args(), restart: :transient),
        worker(OctopusProject.Consumer, consumer_args(), restart: :transient)
      ]
  end
  def supervision_collection(_other_env), do: throw :unsupported_env

  def workers_count, do: 4

  def broker_workers_sup_args do
    [[]]
  end

  def d_storage_args do
    [[]]
  end

  def broker_args do
    [
      %{
        calc_fun: &OctopusProject.ForeignLanguageWrapper.calculate/4,
        conj_fun: &OctopusProject.Conjugator.BasicOverlapConjugator.conjugate_fun/4,
        finale_fun: &OctopusProject.the_end/1,
        workers_count: workers_count()
      }
    ]
  end

  def queue_args do
    [
      %{
        threshold: 1000,
        sub_strategy: :all_for_each
      }
    ]
  end

  def task_gen_args do
    [
      %{
        workers_count:      workers_count(),
        dimensions:         :single,
        dimensionality_d1:  32,
        dimensionality_d2:  0,
        task_division_rule: :equal,
        task_size:          0,
        overlapping_rule:   :overlap,
        overlapping_amount: 3,
        composition_rule:   :none,
        bucket_rule:        :batch,
        d1_prefix:          "line_",
        d2_prefix:          "col_"
      }
    ]
  end

  def ifac_args do
    [
      %{
        filename: "input_file.in",
        d1_prefix: "line_",
      },
      :file,
      nil,
      fn () ->
        OctopusProject.TaskGenerator.init_buckets()
        #OctopusProject.TaskGenerator.start_loop()
        #Agent.start_link(fn -> :erlang.timestamp() end, name: :stopwatch)
      end
    ]
  end

  def consumer_args do
    [
      %{
        consumers_count: 1,
        dequeue_callback: &OctopusProject.Broker.acquire_task/1
      }
    ]
  end

  def the_end(bucket_name) do
    # t = :timer.now_diff(:erlang.timestamp(), Agent.get(:stopwatch, &(&1))) / 1000.0
    {:ok, res} = DStorage.lookup(:comp_results)
    f = File.stream!("result.out")
    res
    |> Enum.sort_by(fn {k, _, _} ->
      Regex.named_captures(~r/[A-Za-z_]+(?<num>[\d]+)/, k)["num"] |> Integer.parse() |> elem(0)
    end)
    |> Stream.map(fn {_, v, _} ->
      v[bucket_name]
    end)
    |> Stream.map(fn l -> Enum.join(l, " ") end)
    |> Enum.into(f)
    Logger.warn("OCTOPUS DONE (in #{t} ms)" )
    Application.stop(:octopus_project)
  end
end
