defmodule OctopusProject.Broker.WorkersSupervisor do
  @moduledoc """
  Module for starting new computational jobs
  """
  use Supervisor
  alias OctopusProject.Worker
  require Logger

  def start_link(args) do
    Supervisor.start_link(__MODULE__, [args], name: __MODULE__)
  end

  def init(_) do
    children = [
      worker(Worker, [], restart: :transient)
    ]
    supervise(children, strategy: :simple_one_for_one)
  end

  def count_workers do
    cnt = Supervisor.count_children(__MODULE__)[:active]
    cnt
  end

  def start_child(args) do
    Logger.info "current work: " <> inspect(Supervisor.which_children(__MODULE__))
    pid = Supervisor.start_child(__MODULE__, [args])
    Logger.info "child started: " <> inspect(pid)
  end

  def stop do
    Logger.info("WorkerSupervisor stopping...")
    Logger.info("Alive processes: #{count_workers()}")
    Logger.info(inspect Supervisor.which_children(__MODULE__))
    Supervisor.stop(__MODULE__)
  end
end
