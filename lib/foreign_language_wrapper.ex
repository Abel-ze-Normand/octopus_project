defmodule OctopusProject.ForeignLanguageWrapper do
  @moduledoc """
  High-level elixir abstraction for calling foreign high-performance computations
  (i.e. for number crunching)
  """

  # require Rustler
  use Rustler, otp_app: :octopus_project, crate: :derichletsolve

  # @max_timeout 12 * 60 * 60 * 1000 # milliseconds

  ##############
  # Public API #
  ##############

  @doc """
  Start calculation delegator. Calls foreign language function
  """
  def calculate(task_data, master_pid, bucket_name, executable_name \\ :native) do
    results = do_calculate(task_data, executable_name, nil)
    send(master_pid, {:done, results, bucket_name})
    :ok
  end

  defp do_calculate(task_data, _executable_name, _func) do
    func_nif(task_data)
  end

  def func_nif(_), do: throw :nif_not_loaded

  # C Port code
  # defp do_calculate(task_data, :native, fun) do
  #   fun.(task_data)
  # end

  # defp do_calculate(_, :native, fun) when is_nil(fun) do
  #   throw :unacceptable_calc_exec
  # end

  # defp do_calculate(task_data, program_name, nil) do
  #   Process.flag(:trap_exit, true)
  #   port = Port.open({:spawn_executable, program_name}, parallelism: true, packet: 4)
  #   wait_status(port)
  # end

  # defp wait_status(port) do
  #   receive do
  #     {:done, data} ->
  #       data
  #     {'EXIT', from, reason} ->
  #       Logger.error("Worker #{inspect(from)} failed due to #{inspect(reason)}")
  #       nil
  #   end
  # end
end
