defmodule OctopusProject.Queue.QueueStateUnit do
  defstruct [:queue, :length, :max_capacity, :subscriptions, :sub_strategy]
end
