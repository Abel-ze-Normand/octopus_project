defmodule OctopusProject.Conjugator.BasicOverlapConjugator do
  alias OctopusProject.{Conjugator}
  require Logger
  use Conjugator
  @moduledoc """
  Executor for tasks conjugation.

  # Conjugation rule:
  if two tasks has shared data, then on conjugation shared data will be sorted and
  taken records with keys having lower order

  ## Example:

  task1: keys: [line_1, line_2, line_3]
  task2: keys: [line_2, line_3, line_4]

  shared_data: [line_2, line_3]
  after conjugation only line_2 and line_3 from task2 will be applied
  """

  @small_number 0.001

  ##############
  # Public API #
  ##############

  @doc """
  ExitMergeReason rule:
    if corresponding values of given bucket data does not trespass some small number,
    then we are ready to be merged

  N.B: we assume that both buckets always natural ordered:
  https://en.wikipedia.org/wiki/Natural_sort_order
  """
  def exit_merge_reason?(bucket_1, bucket_2, shared_keys) do
    shared_data_b1 = unpack_bucket_data_by_keys(bucket_1, shared_keys)
    shared_data_b2 = unpack_bucket_data_by_keys(bucket_2, shared_keys)

    check_vals_to_be_close = fn list ->
      list |> Enum.map(fn {v1, v2} -> abs(v1 - v2) end) |> Enum.max()
    end
    max = apply_to_bucket_corresp_vals(shared_data_b1, shared_data_b2, check_vals_to_be_close) |> Enum.max()
    max < @small_number
  end

  @doc """
  Merge rule:
    bcs buckets are relatively close, we can use use shared data from any of the buckets
    it’s can be represented as [b1_only_keys] ++ [shared_keys_from_b2] ++ [b2_only_keys]
  """
  def merge(bucket_1, bucket_2, merged_name, shared_keys) do
    b1_data = unpack_bucket_data(bucket_1)
    b2_data = unpack_bucket_data(bucket_2)
    b1_data_exclusive = bucket_exclusive(b1_data, shared_keys)

    {:bucket, {merged_name, b1_data_exclusive ++ b2_data}}
  end

  @doc """
  Mutation rule:
    apply contraction for every corresponding value by calculation mean between them
  """
  def mutate_buckets(bucket_1, bucket_2, shared_keys) do
    shared_data_b1 = unpack_bucket_data_by_keys(bucket_1, shared_keys)
    shared_data_b2 = unpack_bucket_data_by_keys(bucket_2, shared_keys)

    # Logger.warn("old data b1: #{inspect shared_data_b1}")
    # Logger.warn("old data b2: #{inspect shared_data_b2}")
    contraction_by_mean = fn list ->
      list |> Enum.map(fn {v1, v2} -> (v1 + v2) / 2.0 end)
    end

    mutated_data =
      Enum.zip(shared_keys, apply_to_bucket_corresp_vals(shared_data_b1, shared_data_b2, contraction_by_mean))

    # Logger.warn("mutated data: #{inspect mutated_data}")
    b1_data = unpack_bucket_data(bucket_1)
    b2_data = unpack_bucket_data(bucket_2)
    b1_data_exclusive = bucket_exclusive(b1_data, shared_keys)
    b2_data_exclusive = bucket_exclusive(b2_data, shared_keys)

    mut_bucket_1 = {:bucket, {unpack_bucket_name(bucket_1), b1_data_exclusive ++ mutated_data}}
    mut_bucket_2 = {:bucket, {unpack_bucket_name(bucket_2), mutated_data ++ b2_data_exclusive}}
    {{:bucket_1, mut_bucket_1}, {:bucket_2, mut_bucket_2}}
  end

  #####################
  # Private functions #
  #####################

  defp unpack_bucket_data_by_keys(b, keys) do
    unpack_bucket_data(b) |> Enum.filter(fn {k, _} -> k in keys end)
  end

  defp unpack_bucket_data({:bucket, {_, data}}), do: data
  defp unpack_bucket_name({:bucket, {name, _}}), do: name

  defp bucket_exclusive(b_data, shared_keys), do: b_data |> Enum.filter(fn {k, _} -> not k in shared_keys end)

  defp apply_to_bucket_corresp_vals(bucket_1_data, bucket_2_data, fun) do
    Stream.zip(bucket_1_data, bucket_2_data)
    |> Stream.map(fn {{_k1, line1}, {_k2, line2}} -> {line1, line2} end)
    |> Stream.map(fn {line1, line2} -> Enum.zip(line1, line2) |> fun.() end)
    |> Enum.into([])
  end
end
