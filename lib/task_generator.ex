defmodule OctopusProject.TaskGenerator do
  @moduledoc """
  Module for task generator component. Full customization read in <<Configurator>> module #TODO

  Algorithm for task generation (without composiotion rules):
  0) ONLY AT START: build buckets info
  1) lookup for all :data tagged records
  2) for each record assign it to corresponding bucket
  3) if bucket full -> push it to queue
  """

  require Logger
  use GenServer
  require Application
  alias OctopusProject.{DStorage,Queue,Grouper,Groupers.OverlapGrouper,Groupers.NoOverlapGrouper,Tree}

  ##############
  # Public API #
  ##############

  @doc """
  Start task generator server. Pass configs from <<configurator>> module #TODO
  Does not start loops, but required to be called once before starting ones
  """
  def start_link(args) do
    default = Application.get_env(:octopus_project, :task_generator_default_args)
    GenServer.start_link(__MODULE__, Map.merge(default, args), name: __MODULE__)
  end

  @doc """
  Stop task generator server.
  """
  def stop() do
    GenServer.stop(__MODULE__)
  end

  @doc """
  Initialize buckets for data dispatching
  """
  def init_buckets() do
    GenServer.call(__MODULE__, :init_buckets)
  end

  @doc """
  By given key of data entry find it’s associated bucket
  """
  def find_bucket(key) do
    GenServer.call(__MODULE__, {:find_bucket, key})
  end

  @doc """
  By given bucket key retrieve associated keys
  """
  def find_data_keys(bucket_key) do
    GenServer.call(__MODULE__, {:find_data_keys, bucket_key})
  end

  @doc """
  Manually push available computation tasks
  Note: not recommended, better use main loop instead
  """
  def push_computation_tasks do
    GenServer.call(__MODULE__, :push_computation_tasks)
  end

  @doc """
  Manually push available conjugation tasks
  Note: not recommended, better use main loop instead
  """
  def push_conjugation_tasks do
    GenServer.call(__MODULE__, :push_conjugation_tasks)
  end

  @doc """
  Starts main loop for retrieving and pushing computational tasks
  """
  def start_loop do
    GenServer.call(__MODULE__, :start_loop)
  end

  @doc """
  Stop task pushing routine
  """
  def stop_loop do
    GenServer.call(__MODULE__, :stop_loop)
  end

  @doc """
  Add new conjugation task (Asynchronous)
  """
  def add_conjugation_task(task) do
    GenServer.cast(__MODULE__, {:add_conjugation_task, task})
  end

  @doc """
  Add task to recomputation
  """
  def add_recomp_task(task) do
    GenServer.cast(__MODULE__, {:add_recomp_task, task})
  end

  @doc """
  Add bucket
  """
  def add_bucket(b) do
    GenServer.cast(__MODULE__, {:add_bucket, b})
  end

  @doc """
  Updates status of given bucket by given status
  """
  def update_bucket_status(bk, st) do
    GenServer.cast(__MODULE__, {:update_bucket_status, bk, st})
  end

  @doc """
  Query for conjugation tree
  """
  @spec get_conj_tree() :: Tree.t
  def get_conj_tree() do
    GenServer.call(__MODULE__, :get_conj_tree)
  end

  @doc """
  Replace old conjugation tree with new
  """
  @spec renew_conj_tree(Tree.t) :: :ok
  def renew_conj_tree(tree) do
    GenServer.cast(__MODULE__, {:renew_conj_tree, tree})
  end

  @doc """
  Save computations payload
  """
  def save_payload(payload, b_name) do
    GenServer.call(__MODULE__, {:save_payload, payload, b_name})
  end

  @doc """
  Query for all available buckets
  """
  @spec get_buckets() :: list
  def get_buckets() do
    {:ok, b} = DStorage.retrieve(:buckets)
    b
  end

  @doc """
  Query for all pushed earlier conjugation tasks
  """
  @spec get_pushed_conj_tasks() :: list
  def get_pushed_conj_tasks() do
    {:ok, t} = DStorage.retrieve(:pushed_conj_tasks)
    t
  end

  @doc """
  Check if there available externally generated conjugation tasks. Always returns list
  """
  @spec check_third_party_conj_tasks() :: list
  def check_third_party_conj_tasks() do
    case DStorage.retrieve(:conj_tasks_generated) do
      {:ok, l}               -> l
      {:error, :eunknownkey} -> []
    end
  end

  #######################
  # GenServer callbacks #
  #######################

  def init(args) do
    Logger.info("Starting Task Generator service...")
    state = %{
      config: args,
      loop_work: false
    }
    Logger.info("Task Generator service successfully started")
    {:ok, state}
  end

  def handle_call(:init_buckets, _from, state) do
    handle_init_buckets(state)
  end

  def handle_call({:find_bucket, key}, _from, state) do
    handle_find_bucket(state, key)
  end

  def handle_call({:find_data_keys, bucket_key}, _from, state) do
    handle_find_data_keys(state, bucket_key)
  end

  def handle_call(:push_computation_tasks, _from, state) do
    handle_push_computation_tasks(state)
  end

  def handle_call(:push_conjugation_tasks, _from, state) do
    handle_push_conjugation_tasks(state)
  end

  def handle_call(:start_loop, _from, state) do
    Logger.info("Started main task generation loop")
    handle_start_loop(state)
  end

  def handle_call(:stop_loop, _from, state) do
    Logger.info("Stopped main task generation loop")
    handle_stop_loop(state)
  end

  def handle_call(:get_conj_tree, _from, state) do
    res = handle_get_conj_tree()
    {:reply, res, state}
  end

  def handle_call({:save_payload, payload, b_name}, _from, state) do
    handle_save_payload(payload, b_name)
    {:reply, :ok, state}
  end

  def handle_cast({:add_recomp_task, task}, state) do
    handle_add_recomp_task(task, state)
  end

  def handle_cast({:add_conjugation_task, task}, state) do
    handle_add_conjugation_task(task, state)
  end

  def handle_cast({:add_bucket, b}, state) do
    handle_add_bucket(b)
    {:noreply, state}
  end

  def handle_cast({:update_bucket_status, bk, st}, state) do
    handle_update_bucket_status(bk, st)
    {:noreply, state}
  end

  def handle_cast({:renew_conj_tree, tree}, state) do
    handle_renew_conj_tree(tree)
    {:noreply, state}
  end

  def handle_info(:loop_comp_tasks, state) do
    # push computational tasks and deferred call itself
    # TODO find more elegant way to loop computations without timers
    # REFACTOR, erlang messages queue will handle order of execution callbacks without specifying
    # its order with timers
    if state[:loop_work] do
      Process.send_after(self(), :push_computation_tasks, 5)
      Process.send_after(self(), :push_conjugation_tasks, 5)
      Process.send_after(self(), :loop_comp_tasks, 10)
    end
    {:noreply, state}
  end

  def handle_info(:push_computation_tasks, state) do
    {:reply, :ok, new_state} = handle_push_computation_tasks(state)
    {:noreply, new_state}
  end

  def handle_info(:push_conjugation_tasks, state) do
    {:reply, :ok, new_state} = handle_push_conjugation_tasks(state)
    {:noreply, new_state}
  end

  def terminate(reason, state) do
    # TODO termination strategies
    super(reason, state)
  end

  #####################
  # Private functions #
  #####################

  defp handle_get_conj_tree() do
    {:ok, t} = DStorage.retrieve(:conj_tree)
    t
  end

  defp handle_renew_conj_tree(tree) do
    :ok = DStorage.store(:conj_tree, tree)
  end

  defp handle_init_buckets(state) do
    config = state[:config]
    chunks = split2chunks(config)
    d1_prefix = config[:d1_prefix]
    bucket_keys = for {bucket, i} <- Stream.with_index(chunks) do
      # TODO shorter bucket_name
      {"bucket_#{i}", (for index <- bucket, do: d1_prefix <> to_string(index)), :generated}
    end
    Logger.warn("initial buckets: #{inspect bucket_keys}")
    renew_buckets(bucket_keys)
    renew_conj_tasks([])
    if config[:overlapping_rule] == :overlap do
      init_conj_tree(bucket_keys)
    end
    {:reply, :ok, state}
  end

  defp init_conj_tree(bucket_keys) do
    keys = bucket_keys |> Enum.map(fn {k, _, _} -> k end)
    conj_tree = Tree.reverse_grow_tree(keys)
    Logger.warn("initial conj tree: #{inspect conj_tree}")
    :ok = DStorage.store(:conj_tree, conj_tree)
  end

  defp handle_find_bucket(state, key) do
    bucket_keys = get_buckets()
    res = case Enum.find(bucket_keys, fn({_bucket_name, keys, _}) -> Enum.member?(keys, key) end) do
            nil                 -> {:error, :enokey}
            {bucket_name, _, _} -> {:ok, bucket_name}
          end
    {:reply, res, state}
  end

  defp handle_find_data_keys(state, bucket_key) do
    buckets_keylist = get_buckets()
    {_, data_keys, _} = Enum.find(buckets_keylist, fn {key, _val, _} -> key == bucket_key end)
    {:reply, data_keys, state}
  end

  defp handle_save_payload(payload, b_name) do
    for line <- payload, do: store_k_v(line, b_name)
  end

  defp store_k_v({key, val}, bucket_name) do
    DStorage.store(
      key,
      # try retrieve existant value
      case DStorage.retrieve(key, :comp_results) do
        {:ok, m} ->
          Map.put(m, bucket_name, val)
        {:error, _} ->
          %{bucket_name => val}
      end,
      :comp_results
    )
  end

  # REFACTOR dry
  defp handle_add_conjugation_task({:conj_task, lb, rb}, state) do
    new_conj_tasks = [{lb, rb}|check_third_party_conj_tasks()] |> Enum.uniq()

    Logger.info("""
    Added new conj task:
    new_conj_tasks: #{inspect new_conj_tasks};
    already_pushed: #{inspect get_pushed_conj_tasks()}
    """)

    Logger.info("new conjugation task: #{inspect {lb, rb}}")

    get_pushed_conj_tasks()
    |> Enum.filter(fn {l, r} ->
      not ((l == lb and r == rb) or (l == rb and r == lb))
    end)
    |> Enum.uniq()
    |> renew_conj_tasks()

    :ok = DStorage.store(:conj_tasks_generated, new_conj_tasks)
    {:noreply, state}
  end

  defp handle_add_conjugation_task({:final, {:conj_task, lb, rb} = finale_task}, state) do
    new_conj_tasks = [{lb, rb}|check_third_party_conj_tasks()] |> remove_duplicate_bucket_pairs()
    :ok = DStorage.store(:conj_tasks_generated, new_conj_tasks)
    :ok = DStorage.store(:finale, finale_task)
    {:noreply, state}
  end

  defp handle_add_recomp_task(task, state) do
    Queue.enqueue(task)
    {:recomp_task, bucket_name} = task
    # [bucket_name | get_buckets()] |> renew_buckets()
    {:noreply, state}
  end

  defp handle_push_computation_tasks(state) do
    bucket_keys = get_buckets()
    to_push_bucket_keys = collect_ready_buckets(bucket_keys)
    for bucket_key <- to_push_bucket_keys do
      # actually we dont need any payload in DStorage for generated task -
      # all we must remember is that it’s actually ready
      DStorage.store(bucket_key, "", :task_comp_pushed)
      Queue.enqueue({:comp_task, bucket_key})
      Logger.info "pushed task #{bucket_key}"
    end
    new_buckets_state = Enum.map bucket_keys, fn (bucket) ->
      update_bucket_status_to_pushed(bucket, to_push_bucket_keys)
    end
    renew_buckets(new_buckets_state)
    {:reply, :ok, state}
  end

  defp handle_push_conjugation_tasks(state) do
    bucket_keys = get_buckets()
    to_push_bucket_keys = collect_ready_conj_buckets(bucket_keys, get_pushed_conj_tasks())
    to_push_3rd_party = collect_ready_3rd_party(get_pushed_conj_tasks())
    tasks = remove_duplicate_bucket_pairs(to_push_bucket_keys ++ to_push_3rd_party)
    for {lb, rb} = bucket_keys_pair <- tasks do
      DStorage.bulk_store(
        [
          {lb, "", :task_conj_pushed},
          {rb, "", :task_conj_pushed}
        ]
      )
      Queue.enqueue({:conj_task, bucket_keys_pair})
      Logger.info "scheduled conjugation for #{elem(bucket_keys_pair, 0)} and #{elem(bucket_keys_pair, 1)}"
    end
    renew_conj_tasks(get_pushed_conj_tasks() ++ tasks)
    {:reply, :ok, state}
  end

  defp remove_duplicate_bucket_pairs(list) do
    Enum.reduce(list, [], fn ({l, r}, acc) ->
      if not {l, r} in acc and not {r, l} in acc do
        [{l, r}|acc]
      else
        acc
      end
    end)
  end

  defp handle_update_bucket_status(bucket_name, status) do
    {:ok, buckets} = DStorage.retrieve(:buckets)
    idx = Enum.find_index(buckets, fn {key, _, _} -> key == bucket_name end)
    new_buckets = buckets |> List.update_at(idx, fn {k, v, _} -> {k, v, status} end)
    :ok = DStorage.store(:buckets, new_buckets)
  end

  # defp get_finalizer() do
  #   case DStorage.retrieve(:finale) do
  #     {:ok, {:final, final_task}} ->
  #       final_task
  #     {:error, :eunknownkey} ->
  #       nil
  #   end
  # end

  # defp finalize({:conj_task, lb, rb}) do

  # end

  defp collect_ready_3rd_party(already_pushed) do
    check_third_party_conj_tasks() |> Enum.filter(&((not &1 in already_pushed) and both_conj_ready?(&1)))
  end

  defp both_conj_ready?({lb, rb}) do
    # check their availability in buckets enumerable
    bucket_exist_and_ready = fn(b_k) ->
      get_buckets()
      |> Enum.any?(fn {k, _, st} -> b_k == k && st == :processed end)
    end
    bucket_exist_and_ready.(lb) and bucket_exist_and_ready.(rb)
  end

  defp collect_ready_conj_buckets(bucket_keys, already_pushed) do
    # TODO
    # naive: not optimized
    conj_tree = handle_get_conj_tree()
    candidates = for l <- bucket_keys, r <- bucket_keys, l != r, do: {l, r}
    candidates
    |> Enum.reduce([], fn {l, r}, acc ->
      if {r, l} in acc do
        acc
      else
        [{l, r}|acc]
      end
    end)
    |> Enum.filter_map(
    fn {{l_b_key, _, _} = l, {r_b_key, _, _} = r} ->
      Tree.same_parent_by_v?(conj_tree, l_b_key, r_b_key) && ready_for_conj?(l, r) && not {l_b_key, r_b_key} in already_pushed
    end,
    fn {{l_b_key, _, _}, {r_b_key, _, _}} ->
      {l_b_key, r_b_key}
    end)
  end

  defp ready_for_conj?(l, r) do
    {_, _, status_l} = l
    {_, _, status_r} = r
    status_l == :processed and status_r == :processed
  end

  defp collect_ready_buckets(bucket_keys) do
    do_collect_ready_buckets(bucket_keys, [])
  end

  defp do_collect_ready_buckets([{bucket_name, keys_for_distribution, :generated}|tail], acc) do
    bucket_ready? = Enum.all? keys_for_distribution, fn (key) ->
      {status, _} = DStorage.retrieve(key, :data)
      status == :ok
    end
    if bucket_ready? do
      do_collect_ready_buckets(tail, [bucket_name|acc])
    else
      do_collect_ready_buckets(tail, acc)
    end
  end

  defp do_collect_ready_buckets([{_bucket_name, _keys_for_distribution, :pushed}|tail], acc) do
    do_collect_ready_buckets(tail, acc)
  end

  defp do_collect_ready_buckets([{_bucket_name, _keys_for_distribution, :processed}|tail], acc) do
    do_collect_ready_buckets(tail, acc)
  end

  defp do_collect_ready_buckets([], acc) do
    acc
  end

  defp handle_start_loop(state) do
    Process.send_after(self(), :loop_comp_tasks, 1000) # after 1 second starts main loop
    {:reply, :ok, %{state| loop_work: true}}
  end

  defp handle_stop_loop(state) do
    {:reply, :ok, %{state| loop_work: false}}
  end

  defp update_bucket_status_to_pushed({key, payload, :generated}, to_push_bucket_keys) do
    if key in to_push_bucket_keys do
      {key, payload, :pushed}
    else
      {key, payload, :generated}
    end
  end

  # just pass
  defp update_bucket_status_to_pushed(x, _) do
    x
  end

  # overlapping grouper

  def split2chunks(config) do
    config |> get_grouper |> Grouper.group
  end

  def get_grouper(config = %{overlapping_rule: :none}) do
    %NoOverlapGrouper{
      range: 0..(config[:dimensionality_d1] - 1),
      workers_count: config[:workers_count]
    }
  end

  def get_grouper(config = %{overlapping_rule:   :overlap,
                             overlapping_amount: overlap_amount,
                             task_devision_rule: :equal}) do
    %OverlapGrouper{
      range: 0..(config[:dimensionality_d1] - 1),
      overlap_amount: overlap_amount,
      workers_count: config[:workers_count],
      strategy: :fair
    }
  end

  def get_grouper(config = %{overlapping_rule:   :overlap,
                             overlapping_amount: overlap_amount,
                             task_size:          task_size,
                             task_devision_rule: :constant}) do
    %OverlapGrouper{
      range: 0..(config[:dimensionality_d1] - 1),
      overlap_amount: overlap_amount,
      per_bucket: task_size,
      strategy: :constant
    }
  end

  defp handle_add_bucket({_, _, _} = b) do
    [b|get_buckets()] |> renew_buckets()
  end

  @doc """
  Replace old buckets collection with new
  """
  @spec renew_buckets(list) :: :ok
  defp renew_buckets(new_state) do
    :ok = DStorage.store(:buckets, new_state)
  end

  @doc """
  Replace old pushed conjugation tasks with new
  """
  @spec renew_conj_tasks(list) :: :ok
  defp renew_conj_tasks(new_state) do
    :ok = DStorage.store(:pushed_conj_tasks, new_state)
  end

end
