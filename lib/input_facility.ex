defmodule OctopusProject.InputFacility do
  alias OctopusProject.DStorage
  use GenServer
  require Logger
  @moduledoc """
  Component for processing user input data
  Supports 3 strategies for collecting data and pushing them in DStorage:
        file - read contents from input file (manual tweaking must be
        performed in <<Configurator>> module). Processing is asynchronous,
        so do not forget to receive packet with results of file parsing (check
        InputFacility.process_file/1 function for protocol details) if you want to be
        100% sure about ending procesing
        function - constant execution of given function must produce next
        batch of processing data, mechanism is similar to iterator (work
        on demand)
        dynamic - start server to accept data feeding and further pushing
        to DStorage.
  First 2 strategies stops server at some point and exit gracefully,
  third one works forever
  """
  @type option_spec :: :file | :function | :dynamic

  ##############
  # Public API #
  ##############

  @doc """
  Start processing routine.
  args :: generic parameters
  option :: pass :file, :function, :dynamic based on your implementation
  """
  @spec start_link(Map.t, option_spec) :: {:ok, pid}
  def start_link(args, option \\ :file, starter_pid \\ nil, done_callback \\ nil) do
    GenServer.start_link(__MODULE__, [args, option, starter_pid, done_callback], name: __MODULE__)
  end

  @doc """
  Convert given string to list of floats
  str :: input string
  """
  @spec str_to_list_f(String.t) :: [Float.t]
  def str_to_list_f(str, delimeter \\ ~r/\s/) do
    Regex.split(delimeter, str, [trim: true]) |> Enum.map(&String.to_float/1)
  end

  @doc """
  Parse file line by line and push them in correct format to DStorage
  """
  def process_file(args) do
    delim = get_in(args, [:args, :delimeter]) || ~r/\s/
    File.stream!(get_in(args, [:args, :filename]))
    |> Stream.map(fn l -> str_to_list_f(l, delim) end)
    |> Stream.with_index()
    |> Stream.map(fn x -> cons_d_storage_record(x, args[:args]) end)
    |> Stream.map(fn {k, v, d} -> DStorage.store(k, v, d) end)
    |> Stream.run()
  end

  #######################
  # GenServer callbacks #
  #######################

  # TODO define init for 2 other strategies
  def init([args, :file, starter_pid, done_callback]) do
    state = %{
      args: args,
      mode: :file,
      starter_pid: starter_pid,
      done_callback: done_callback
    }
    if starter_pid do
      Process.monitor(starter_pid)
    end
    Process.send_after(__MODULE__, :process_file, 500)
    {:ok, state}
  end

  def init([args, :dynamic, starter_pid, _]) do
    state = %{
      args: args,
      mode: :file,
      starter_pid: starter_pid
    }
    if starter_pid do
      Process.monitor(starter_pid)
    end
    {:ok, state}
  end

  def handle_info(:process_file, state) do
    process_file(state)
    notify_end_processing(state)
    GenServer.stop(__MODULE__)
    {:noreply, :ok}
  end

  # callback on dying starting process
  def handle_info({:DOWN, _ref, :process, pid, reason}, _state) do
    Logger.warn("InputFacility registered failing of monitored starter process(#{inspect pid}) due to reason #{inspect reason}")
    GenServer.stop(__MODULE__, :monitored_process_failed)
  end

  #####################
  # Private functions #
  #####################

  defp cons_d_storage_record({line_data, i}, args) do
    {args[:d1_prefix] <> to_string(i), line_data, :data}
  end

  defp notify_end_processing(state) do
    if state[:starter_pid] do
      send state[:starter_pid], :ifac_file_processed
    end
    if state[:done_callback] do
      state[:done_callback].()
    end
  end
end
