defmodule OctopusProject.Broker do
  @moduledoc """
  Task decryptor and worker job assigner
  """
  use GenServer
  require Logger
  alias OctopusProject.{Broker.WorkersSupervisor,DStorage,TaskGenerator}

  ##############
  # Public API #
  ##############

  @doc """
  Initialize broker genserver.
  args :: bypass generic args, :workers_count and :calc_fun is obsolete
  """

  def start_link(args) do
    # {:ok, _} = WorkersSupervisor.start_link(args)
    {:ok, _} = GenServer.start_link(__MODULE__, args, name: __MODULE__)
  end

  @doc """
  Take task for computations
  """
  def acquire_task(task) do
    GenServer.call(__MODULE__, {:acquire_task, task})
  end

  @doc """
  Stop broker process
  """
  def stop do
    GenServer.stop(__MODULE__)
  end

  #######################
  # GenServer callbacks #
  #######################

  def init(args) do
    {:ok, args}
  end

  def handle_call({:acquire_task, task}, _from, state) do
    handle_acquire_task(task, state)
  end

  def handle_info({:acquire_task, task}, state) do
    handle_acquire_task(task, state)
    {:noreply, state}
  end

  def terminate(reason, _) do
    Logger.info("Broker termination due to reason: #{inspect reason}")
    WorkersSupervisor.stop()
  end

  #####################
  # Private functions #
  #####################

  defp handle_acquire_task(task = {:comp_task, bucket_key}, state) do
    # guard
    already_working_jobs = WorkersSupervisor.count_workers
    status = if already_working_jobs <= state[:workers_count] do
      {:ok, task_data} = gather_task_data(task)
      Logger.info("starting calc job for #{inspect(task)} task")
      pid_status = WorkersSupervisor.start_child(
        %{
            task_data: task_data,
            fun: state[:calc_fun],
            bucket_names: [bucket_key]
        })
      Logger.info("#{inspect task} : #{inspect pid_status}")
      :ok
    else
      Logger.debug("max workers, delayed")
      send(__MODULE__, {:acquire_task, task})
      :ok_delayed
    end
    {:reply, status, state}
  end

  defp handle_acquire_task(task = {:recomp_task, bucket_key}, state) do
    # guard
    already_working_jobs = WorkersSupervisor.count_workers
    status = if already_working_jobs <= state[:workers_count] do
      {:ok, task_data} = gather_task_data(task)
      Logger.info("starting recomputation job for #{inspect(task)} task")
      pid_status = WorkersSupervisor.start_child(
        %{
            task_data: task_data,
            fun: state[:calc_fun],
            bucket_names: [bucket_key]
        })
      Logger.info("#{inspect task} : #{inspect pid_status}")
      :ok
    else
      Logger.debug("max workers, delayed")
      send(__MODULE__, {:acquire_task, task})
      :ok_delayed
    end
    {:reply, status, state}
  end

  defp handle_acquire_task(task = {:conj_task, {bucket_1_name, bucket_2_name}}, state) do
    already_working_jobs = WorkersSupervisor.count_workers
    status = if already_working_jobs <= state[:workers_count] do
      {:ok, task_data} = gather_task_data(task)
      Logger.info("starting conj job for #{inspect(task)} task")
      pid_status = WorkersSupervisor.start_child(
        %{
            task_data: task_data,
            fun: state[:conj_fun],
            bucket_names: [bucket_1_name, bucket_2_name],
            finale_fun: state[:finale_fun]
        })
      Logger.info("#{inspect task} : #{inspect pid_status}")
      :ok
    else
      Logger.debug("max workers, delayed")
      send(__MODULE__, {:acquire_task, task})
      :ok_delayed
    end
    {:reply, status, state}
  end

  defp gather_task_data({:comp_task, bucket_key}) do
    data_keys_list = TaskGenerator.find_data_keys(bucket_key)
    {:ok, res} = DStorage.bulk_retrieve(data_keys_list, :data)
    data = clean_dstorage_tags(res)
    {:ok, data}
  end

  defp gather_task_data({:recomp_task, bucket_key}) do
    data_keys_list = TaskGenerator.find_data_keys(bucket_key)
    # worker after execution places it’s results in a map with structure:
    # %{bucket_name => value}
    # so we keep same key for multiple data in multiple buckets
    {:ok, res} = DStorage.bulk_retrieve(data_keys_list, :comp_results)
    data = clean_dstorage_tags(res) |> Enum.map(fn {k, v} -> {k, v[bucket_key]} end)
    {:ok, data}
  end

  defp gather_task_data({:conj_task, {bucket_1_k, bucket_2_k}}) do
    # retrieve shared data
    data_keys_list_1 = TaskGenerator.find_data_keys(bucket_1_k)
    data_keys_list_2 = TaskGenerator.find_data_keys(bucket_2_k)
    {:ok, res_b1} = DStorage.bulk_retrieve(data_keys_list_1, :comp_results)
    {:ok, res_b2} = DStorage.bulk_retrieve(data_keys_list_2, :comp_results)
    b1_data = clean_dstorage_tags(res_b1) |> Enum.map(fn {k, v} -> {k, v[bucket_1_k]} end)
    b2_data = clean_dstorage_tags(res_b2) |> Enum.map(fn {k, v} -> {k, v[bucket_2_k]} end)
    lb = {:bucket, {bucket_1_k, b1_data}}
    rb = {:bucket, {bucket_2_k, b2_data}}
    {:ok, %{conj_task_data: {lb, rb}, conj_tree: TaskGenerator.get_conj_tree()}}
  end

  defp gather_task_data({:conj_task, :final}) do
    # TODO create finalizer task
    nil
  end

  defp gather_task_data(_) do
    {:error, :unsupported}
  end

  defp clean_dstorage_tags(data) when is_list(data) do
    Enum.map(data, fn {key, value, _tag} -> {key, value} end)
  end
end
