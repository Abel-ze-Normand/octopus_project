defmodule OctopusProject.Queue do
  use GenServer
  require Logger
  require Application
  alias OctopusProject.Queue.QueueStateUnit, as: QueueStateUnit

  @moduledoc """
  Queue server.
  """

  ##############
  # Public Api #
  ##############

  @doc """
  Start queue server.
  Optional parameters:
  :threshold - max queue capacity
  :sub_strategy = :all_for_each | :each_uniq - strategy for notifying subscribers
  """
  def start_link(args \\ Application.get_env(:octopus_project, :queue_default_args)) do
    GenServer.start_link(__MODULE__, args, name: __MODULE__)
  end

  @doc """
  Stop queue server
  """
  def stop do
    GenServer.stop(__MODULE__)
  end

  @doc """
  Insert new item into head of queue
  """
  def enqueue(item) do
    GenServer.call(__MODULE__, {:enqueue, item})
  end

  @doc """
  Pop item from tail of the queue
  """
  def dequeue do
    GenServer.call(__MODULE__, {:dequeue})
  end

  #######################
  # GenServer callbacks #
  #######################

  def init(args) do
    Logger.info "Starting queue service"
    queue_state_unit = %QueueStateUnit{
      queue:         :queue.new,
      length:        0,
      max_capacity:  args[:threshold],
      subscriptions: [],
      sub_strategy:  args[:sub_strategy]
    }
    Logger.info "Queue service successfully started"
    {:ok, queue_state_unit}
  end

  def handle_call({:enqueue, item}, _from, state) do
    handle_enqueue(item, state)
  end

  def handle_call({:dequeue}, _from, state) do
    handle_dequeue(state)
  end

  def terminate(reason, state) do
    # TODO backup terminated tasks OR
    # give this responsibility to task_generator
    # case reason do
    #   :normal   -> :ok
    #   :shutdown -> :ok
    #   _         ->
    # end
    super(reason, state)
  end

  #####################
  # Private functions #
  #####################

  defp handle_enqueue(
    item,
    %QueueStateUnit{queue: queue, length: length, max_capacity: max_capacity} = state
  ) when length < max_capacity do
    state = %{state | queue: :queue.in(item, queue), length: length + 1}
    {:reply, {:ok, max_capacity - length - 1}, state}
  end

  defp handle_enqueue(_item, state), do: {:reply, {:error, :efullq}, state}

  defp handle_dequeue(
    %QueueStateUnit{queue: queue, length: length} = state
  ) when length > 0 do
    {{:value, item}, new_queue} = :queue.out(queue)
    state = %{state | queue: new_queue, length: length - 1}
    {:reply, item, state}
  end

  defp handle_dequeue(state), do: {:reply, {:error, :eemptyq}, state}
end
