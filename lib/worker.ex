defmodule OctopusProject.Worker do
  use GenServer
  require Logger
  alias OctopusProject.{DStorage, TaskGenerator}
  @moduledoc """
  Module for primitive watching for external computational processes.
  Implement any function with description NAME(task_info, this_module_pid) for
  computations and send results of your computations to ’this_module_pid’
  """

  ##############
  # Public API #
  ##############

  @doc """
  Generally you don’t have to manually call this function, leave it for supervisor
  """
  def start_link(args) do
    Logger.info("New worker starting..")
    {:ok, pid} = GenServer.start_link(__MODULE__, args)
    task_data = args[:task_data]
    fun = args[:fun]
    executable_name = Map.get(args, :executable, :native)
    # there must be exactly one elem for comp tasks, for conj > 1
    bucket_names = args[:bucket_names]
    Logger.info "worker spawned: " <> inspect(spawn_link(fn -> Process.link(pid); fun.(task_data, pid, bucket_names, executable_name) end))
    {:ok, pid}
  end

  #######################
  # GenServer callbacks #
  #######################

  def init(args) do
    {:ok, args}
  end

  @doc """
  Handler for ending computations and this process terminator
  """
  def handle_info({:done, data, [bucket_name]}, state) when is_list(data) do
    TaskGenerator.save_payload(data, bucket_name)
    TaskGenerator.update_bucket_status(bucket_name, :processed)
    Logger.info("Worker ended")
    {:stop, :normal, state}
  end

  def handle_info({:done, data, [bucket_name]}, state) when is_tuple(data) do
    TaskGenerator.save_payload([data], bucket_name)
    TaskGenerator.update_bucket_status(bucket_name, :processed)
    Logger.info("Worker ended")
    {:stop, :normal, state}
  end

  @doc """
  Handler for ending conjugations and this process terminator
  data: map of new bucket_names (1 key - if conjugation successful and >1 if conjugation mutated values)
  """
  def handle_info({:conj_done, %{status: :merged} = data, bucket_names}, state) do
    save_merged_bucket(data)
    send_task_manager_conj_task(data)
    Logger.info("Conjugation for buckets #{Enum.join(bucket_names, ", ")} performed. Merged !!!")
    if thats_all?(bucket_names) do
      # TODO output facility?
      # state[:finale_fun].(bucket_names)
      [{:bucket, {last_bucket, _}}] = data[:buckets]
      state[:finale_fun].(last_bucket)
    end
    {:stop, :normal, state}
  end

  def handle_info({:conj_done, %{status: :unmerged} = data, bucket_names}, state) do
    save_mutated_buckets(data)
    send_task_manager_recomputation_task(data)
    send_task_manager_conj_task(data)
    Logger.info("Conjugation for buckets #{Enum.join(bucket_names, ", ")} not performed. Unmerged")
    {:stop, :normal, state}
  end

  def terminate(reason, _) do
    Logger.info("Worker #{inspect self()} terminated!")
    # Process.sleep(2000)
    if reason != :normal do
      Logger.error("worker terminated due to reason #{inspect reason}")
    end
  end

  # TODO correct?
  def thats_all?([lb, rb]) do
    DStorage.retrieve(:finale) |> check_finale(lb, rb)
  end

  #####################
  # Private functions #
  #####################

  defp check_finale({:ok, {:conj_task, tlb, trb}}, lb, rb), do: (tlb == lb && trb == rb) || (tlb == rb && trb == lb)
  defp check_finale({:error, :eunknownkey}, lb, rb), do: false

  defp save_merged_bucket(%{buckets: [{:bucket, {b_name, payload}}]}) do
    keys = for {k, _} <- payload, do: k
    new_bucket = {b_name, keys, :processed}
    TaskGenerator.save_payload(payload, b_name)
    TaskGenerator.add_bucket(new_bucket)
  end

  defp send_task_manager_recomputation_task(%{buckets: [{:bucket, {b1_name, _}}, {:bucket, {b2_name, _}}]}) do
    TaskGenerator.add_recomp_task(cons_recomp_task(b1_name))
    TaskGenerator.add_recomp_task(cons_recomp_task(b2_name))
    TaskGenerator.update_bucket_status(b1_name, :pushed)
    TaskGenerator.update_bucket_status(b2_name, :pushed)
  end

  defp cons_recomp_task(bucket_name) do
    {:recomp_task, bucket_name}
  end

  defp save_mutated_buckets(%{buckets: [{:bucket, {b1_name, payload1}}, {:bucket, {b2_name, payload2}}]}) do
    TaskGenerator.save_payload(payload1, b1_name)
    TaskGenerator.save_payload(payload2, b2_name)
  end

  defp send_task_manager_conj_task(%{conj_task: nil}) do
    nil
  end

  defp send_task_manager_conj_task(%{conj_task: next_conj_task, conj_tree: new_conj_tree}) do
    TaskGenerator.add_conjugation_task(next_conj_task)
    TaskGenerator.renew_conj_tree(new_conj_tree)
  end

end
