defmodule OctopusProject.Consumer do
  @moduledoc """
  Queue consumer service. Pass in <<configurator>>’s manifest number of queue consumers
  """
  require Logger
  alias OctopusProject.{Queue,Broker}

  ##############
  # Public API #
  ##############

  @doc """
  Start consumer subprocesses.
  args :: bypass parameters for consumers.
        :consumers_count - number of consuming processes
        :dequeue_callback - function to process data
  """
  def start_link(args) do
    pid = spawn_link fn ->
      consumers_count = Map.get(args, :consumers_count, 1)
      fun = Map.get(args, :dequeue_callback, fn x -> x end)
      pids_list = for _ <- 1..consumers_count do
        spawn_link(__MODULE__, :start_consumer, [fun, :batched])
      end

      if Process.whereis(:consumer_pids_list) do
        Agent.update(:consumer_pids_list, fn(_) -> pids_list end, name: :consumer_pids_list)
      else
        Agent.start_link(fn -> pids_list end, name: :consumer_pids_list)
      end
    end
    {:ok, pid}
  end

  @doc """
  Abstraction over executing consuming process. Also can be used
  to start individual processes
  """
  def start_consumer(callback, :batched) do
    Logger.info "Start consuming..."
    loop(callback, false)
  end

  def start_consumer(callback) do
    Logger.info "Started consuming individual process"
    pid = spawn_link(__MODULE__, :start_consumer, [callback, :batched])
    if Process.whereis(:consumer_pids_list) do
      Agent.update(:consumer_pids_list, fn(state) -> (unless (pid in state), do: [pid|state]) end, name: :consumer_pids_list)
    else
      Agent.start_link(fn -> [pid] end, name: :consumer_pids_list)
    end
  end

  @doc """
  Stop all consumers processes
  """
  def stop do
    pids_list = Agent.get(:consumer_pids_list, &(&1))
    for pid <- pids_list, do: send(pid, :stop_consumer)
    Agent.stop(:consumer_pids_list)
    :ok
  end

  #####################
  # Private functions #
  #####################

  defp loop(callback, false) do
    stop_flag = receive do
      :stop_consumer ->
        true
    after 1 ->
        false
    end
    case Queue.dequeue do
      {:error, :eemptyq} ->
        nil
      payload ->
        callback.(payload)
    end
    loop(callback, stop_flag)
  end

  defp loop(_, true) do
    Logger.info("Consumer stopped")
    nil
  end

  def routine(payload) do
    # generic interface ?
    Broker.acquire_task(payload)
  end
end
