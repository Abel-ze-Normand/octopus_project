FROM elixir:1.4.2
MAINTAINER Abel Normand

ARG nodename
ENV VERSION 0.1.0

COPY _build/prod/rel/octopus_project/releases/$VERSION/ /octopus_project/
WORKDIR /octopus_project/

RUN echo "-sname $nodename
-cookie cookie
-smp auto
" > vm.args

ENTRYPOINT bin/octopus_project.sh console
