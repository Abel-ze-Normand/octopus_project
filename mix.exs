defmodule OctopusProject.Mixfile do
  use Mix.Project

  def project do
    [app: :octopus_project,
     version: "0.1.0",
     elixir: "~> 1.3",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     compilers: [:rustler|Mix.compilers],
     test_coverage: [tool: ExCoveralls],
     preferred_cli_env: ["coveralls": :test, "coveralls.detail": :test, "coveralls.post": :test, "coveralls.html": :test],
     rustler_crates: [
       derichletsolve: [
         path: "native/derichletsolve", # relative to project root
         mode: (if Mix.env == :prod, do: :release, else: :debug)
       ]
     ],
     deps: deps()]
  end

  def application do
    [
      applications: [
        :amnesia,
        :rustler,
        :logger,
        :edeliver,
        :cowboy
      ],
      mod: {OctopusProject, []},
      registered: [:octopus_project],
      # TODO remove ALL envs, leave it on the <<Configurator>>
      env: [
        queue_default_args: %{
          threshold: 1000,
          sub_strategy: :all_for_each
        },
        task_generator_default_args: %{
          workers_count:      5,
          dimensions:         :single,
          dimensionality_d1:  10,
          dimensionality_d2:  0,
          task_devision_rule: :equal,
          overlapping_rule:   :none,
          composition_rule:   :none,
          bucket_rule:        :batch,
          d1_prefix:          "line_",
          d2_prefix:          "col_"
        },
        exec_func_name: "derichletsolve",
        environ: Mix.env()
      ]
    ]
  end

  defp deps do
    [
      {:edeliver, "~> 1.4"},
      {:distillery, "~> 1.3", warn_missing: false},
      {:cowboy, "~> 1.1"},
      {:amnesia, "~> 0.2.7"},
      # {:amnesia, github: "meh/amnesia", tag: :master},
      {:excoveralls, "~> 0.6", only: :test},
      # {:rustler, github: "hansihe/rustler", tag: :master, sparse: "rustler_mix"}
      {:rustler, "~> 0.9.0"}
    ]
  end
end
