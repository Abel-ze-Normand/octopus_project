pub fn solve_derichlet(arr: Vec<Vec<f32>>, f: &Fn(f32, f32) -> f32) -> Vec<Vec<f32>> {
    let h = 0.1;
    let prev_res = arr;
    let res: Vec<Vec<f32>>;
    let eps = 1e-8;
    let mut prev_e = 10e10;
    loop {
        let (ret_res, e) = map_sector(&prev_res, h, &f);
        if (prev_e - e).abs() < eps {
            res = ret_res;
            break;
        }
        prev_e = e;
    }
    res
}

// arr
/*
Method for single Poisson iteration
:params:
arr: input array
h: step
f: right-side function of Laplas’ equation

:return:
tuple(new array, max diff)
*/
fn map_sector(arr: &Vec<Vec<f32>>, h: f32, f: &Fn(f32, f32) -> f32) -> (Vec<Vec<f32>>, f32) {
    let n = arr.len();
    let m = arr[0].len();
    let mut result = vec![vec![0.0; m]; n];
    let mut max_diff = 1e-8;
    for (i, subarr) in arr.iter().enumerate() {
        for (j, _) in subarr.iter().enumerate() {
            // guard
            if i == n - 1 || i == 0 {
                result[i][j] = arr[i][j];
                continue;
            }
            if j == m - 1 || j == 0 {
                result[i][j] = arr[i][j];
                continue;
            }
            let x = j as f32 * h;
            let y = i as f32 * h;
            result[i][j] = (arr[i + 1][j] + arr[i - 1][j] + arr[i][j + 1] + arr[i][j - 1]) * 0.25 - h * h * f(x, y);
            if (result[i][j] - arr[i][j]).abs() > max_diff {
                max_diff = (result[i][j] - arr[i][j]).abs();
            }
        }
    }
    (result, max_diff)
}
