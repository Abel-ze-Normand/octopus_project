typedef unsigned char byte;

int read_exact(byte *, int);
int write_exact(byte *, int);
int read_cmd(byte *);
int write_cmd(byte *, int);
