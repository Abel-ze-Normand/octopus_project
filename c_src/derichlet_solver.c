#include "erl_interface.h"
#include "ei.h"
#include "erl_comm.h"
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#define CREATE_ARRAY(type, length) (type*)malloc(sizeof(type) * length)

int main() {
  ETERM *tuplep, *fnp, *argp;
  byte buffer[1000000];

  erl_init(NULL, 0);

  assert(read_cmd(buffer) != 0);

  tuplep = erl_decode(buffer);
  fnp = erl_element(0, tuplep);
  argp = erl_element(1, tuplep);

  assert(strcmp(ERL_ATOM_PTR(fnp), "compute"));


}

double ** derichlet_solve(double **arr, int n, int m) {
  double ** result = CREATE_ARRAY(double*, n);
  for (int i = 0; i < n; i++) {
    result[i] = CREATE_ARRAY(double, m);
  }
}
