defmodule TaskGeneratorTest do
  use ExUnit.Case, async: false
  doctest OctopusProject.TaskGenerator
  alias OctopusProject.{TaskGenerator, DStorage, Queue}

  @task_generator_opts %{
    workers_count:      5,
    dimensionality_d1:  10,
    dimensions:         :single,
    task_devision_rule: :equal,
    overlapping_rule:   :none,
    composition_rule:   :none,
    bucket_rule:        :batch
  }

  @overlap_task_generator_opts %{
    workers_count:      2,
    dimensionality_d1:  6,
    dimensions:         :single,
    task_devision_rule: :equal,
    overlapping_rule:   :overlap,
    overlapping_amount: 1,
    composition_rule:   :none,
    bucket_rule:        :batch
  }

  test "key of record can be determined its bucket" do
    TaskGenerator.start_link(@task_generator_opts)
    DStorage.start_link
    TaskGenerator.init_buckets

    test_data = for x <- 0..(@task_generator_opts[:dimensionality_d1]-1) do
      {"line_#{x}", :somedata, :data}
    end

    per_bucket = div(@task_generator_opts[:dimensionality_d1], @task_generator_opts[:workers_count])
    for {key, _payload, _tag} <- test_data do
      [num] = Regex.run(~r/\d$/, key)
      {int_num, _} = Integer.parse(num)
      right_num = div(int_num, per_bucket)
      assert TaskGenerator.find_bucket(key) == {:ok, "bucket_#{right_num}"}
    end

    TaskGenerator.stop
  end

  test "data keys can be restored by bucket key" do
    TaskGenerator.start_link(@task_generator_opts)
    DStorage.start_link
    Queue.start_link

    TaskGenerator.init_buckets
    ["line_0", "line_1"] = TaskGenerator.find_data_keys("bucket_0")

    Queue.stop
    DStorage.stop
    TaskGenerator.stop
  end

  test "overlapping keys" do
    TaskGenerator.start_link(@overlap_task_generator_opts)
    DStorage.start_link
    Queue.start_link

    TaskGenerator.init_buckets
    # [[0, 1, 2], [2, 3, 4], [4, 5]]

    assert ["line_0", "line_1", "line_2"] == TaskGenerator.find_data_keys("bucket_0")
    assert ["line_2", "line_3", "line_4"] == TaskGenerator.find_data_keys("bucket_1")
    assert ["line_4", "line_5"] == TaskGenerator.find_data_keys("bucket_2")

    Queue.stop
    DStorage.stop
    TaskGenerator.stop
  end

  test "pushes ready computation tasks to queue without main pushing loop" do
    TaskGenerator.start_link(@task_generator_opts)
    DStorage.start_link
    Queue.start_link

    # no needed records in d_storage => none of buckets is ready
    TaskGenerator.init_buckets
    TaskGenerator.push_computation_tasks
    assert {:error, :eemptyq} == Queue.dequeue

    # bcs each test bucket contains pairs like [line_0, line_1] and so on,
    # if we have keys line_0, line_2, ... buckets will not be fullfilled
    # => none of buckets is ready
    list = for i <- [0, 2, 4, 6, 8] do
      {"line_#{i}", :somedata, :data}
    end
    DStorage.bulk_store(list)
    TaskGenerator.push_computation_tasks
    assert {:error, :eemptyq} == Queue.dequeue

    # ready everything => push all 5 test buckets
    second_list = for i <- [1, 3, 5, 7, 9] do
      {"line_#{i}", :somedata, :data}
    end
    DStorage.bulk_store(second_list)
    TaskGenerator.push_computation_tasks
    for _i <- 0..4 do
      {:comp_task, _bucket_name} = Queue.dequeue
    end

    Queue.stop
    DStorage.stop
    TaskGenerator.stop
  end

  test "pushes ready conjugation tasks to queue" do
    TaskGenerator.start_link(@overlap_task_generator_opts)
    DStorage.start_link
    Queue.start_link

    TaskGenerator.init_buckets

    # task generator implicitly depends for data from DStorage. for
    # test we can "hack" business logic and inject(modify) our wanted result
    {:ok, buckets} = DStorage.retrieve(:buckets)
    [first|[second|tail]] = buckets
    {f_b_name, f_data_keys, _} = first
    {s_b_name, s_data_keys, _} = second
    fabricated_buckets = [{f_b_name, f_data_keys, :processed}|[{s_b_name, s_data_keys, :processed}|tail]]
    :ok = DStorage.store(:buckets, fabricated_buckets)

    TaskGenerator.push_conjugation_tasks

    assert {:conj_task, {f_b_name, s_b_name}} == Queue.dequeue

    Queue.stop
    DStorage.stop
    TaskGenerator.stop
  end

end
