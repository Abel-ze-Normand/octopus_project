defmodule ForeignLanguageWrapperTest do
  use ExUnit.Case, async: false
  alias OctopusProject.ForeignLanguageWrapper
  doctest ForeignLanguageWrapper
  require Logger

  test "perform computations on NIF" do
    task_data = [
      {"line0", [1.0, 1.0, 1.0, 1.0]},
      {"line1", [2.0, 0.0, 0.0, 2.0]},
      {"line2", [3.0, 0.0, 0.0, 2.0]},
      {"line3", [4.0, 4.0, 4.0, 4.0]}
    ]

    ForeignLanguageWrapper.calculate(task_data, self(), "bucket_0")

    assert_receive {:done, transformed_data, ["bucket_0"]}

    transformed_data |> inspect |> Logger.debug
  end
end
