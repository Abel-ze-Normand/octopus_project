defmodule DStorageTest do
  use ExUnit.Case, async: false
  doctest OctopusProject.DStorage
  alias OctopusProject.DStorage

  test "saves data" do
    DStorage.start_link
    :ok = DStorage.store("key", "test_data")
    assert {:ok, "test_data"} == DStorage.retrieve("key")
    DStorage.stop
  end

  test "answers to unknown queries" do
    DStorage.start_link
    :ok = DStorage.store("key", "test_data")
    assert {:error, :eunknownkey} == DStorage.retrieve("key1")
    DStorage.stop
  end

  test "same queries return :ok" do
    DStorage.start_link
    :ok = DStorage.store("key", "test_data")
    assert {:ok, "test_data"} == DStorage.retrieve("key")
    assert {:ok, "test_data"} == DStorage.retrieve("key")
    DStorage.stop
  end

  test "bulk retrieve takes exact records" do
    DStorage.start_link
    test_data = [
      {"key1", :some_data_1, :tag1},
      {"key2", :some_data_2, :tag1},
      {"key3", :some_data_3, :tag1},
      {"key4", :some_data_4, :tag1},
      {"key5", :some_data_5, :tag2}
    ]
    origin = [
      {"key1", :some_data_1, :tag1},
      {"key2", :some_data_2, :tag1},
      {"key3", :some_data_3, :tag1},
    ]
    DStorage.bulk_store(test_data)
    {:ok, res} = DStorage.bulk_retrieve(["key1", "key2", "key3"], :tag1)
    for r <- res do
      Enum.member?(origin, r)
    end
    DStorage.stop
  end

  test "retrieve_and_forget to same query return :error, :eunknownkey" do
    DStorage.start_link
    :ok = DStorage.store("key", "test_data")
    assert {:ok, "test_data"} == DStorage.retrieve_and_forget("key")
    assert {:error, :eunknownkey} == DStorage.retrieve("key")
    DStorage.stop
  end

  test "tag data" do
    DStorage.start_link
    :ok = DStorage.store("key", "test_data", :tag1)
    assert {:error, :eunknownkey} == DStorage.retrieve("key")
    assert {:ok, "test_data"} == DStorage.retrieve("key", :tag1)
    DStorage.stop
  end

  test "forget data" do
    DStorage.start_link
    :ok = DStorage.store("key", "test_data", :tag)
    assert :ok == DStorage.forget("key", :tag)
    assert {:error, :eunknownkey} == DStorage.retrieve("key")
    assert {:error, :eunknownkey} == DStorage.retrieve("key", :tag)
    DStorage.stop
  end

  test "bulk store data (list)" do
    DStorage.start_link
    test_data = [
      {"key1", :some_data},
      {"key2", {:some, :data}},
      {"key3", :some_data_1, :tag1}
    ]
    :ok = DStorage.bulk_store(test_data)
    assert {:ok, :some_data} = DStorage.retrieve("key1")
    assert {:ok, {:some, :data}} = DStorage.retrieve("key2")
    assert {:error, :eunknownkey} = DStorage.retrieve("key3")
    assert {:ok, :some_data_1} = DStorage.retrieve("key3", :tag1)
    DStorage.stop
  end

  test "bulk store data (NOT list)" do
    DStorage.start_link
    test_data = :something
    assert {:error, :enolist} == DStorage.bulk_store(test_data)
    DStorage.stop
  end

  test "lookup by tag" do
    DStorage.start_link
    test_data = [
      {"key1", :some_data_1, :tag1},
      {"key2", :some_data_2, :tag2},
      {"key3", :some_data_3, :tag1}
    ]

    :ok = DStorage.bulk_store(test_data)

    expected_result_tag1 = [
      {"key1", :some_data_1, :tag1},
      {"key3", :some_data_3, :tag1}
    ]
    expected_result_tag2 = [
      {"key2", :some_data_2, :tag2}
    ]
    expected_result_tag3 = {:error, :enotag}

    {:ok, results_1} = DStorage.lookup(:tag1)
    assert Enum.count(results_1) == Enum.count(expected_result_tag1)
    for x <- results_1 do
      assert Enum.member?(expected_result_tag1, x)
    end

    {:ok, results_2} = DStorage.lookup(:tag2)
    assert Enum.count(results_2) == Enum.count(expected_result_tag2)
    for x <- results_2 do
      assert Enum.member?(expected_result_tag2, x)
    end

    assert expected_result_tag3 == DStorage.lookup(:tag3)
    DStorage.stop
  end

  test "bulk retrieve and forget" do
    DStorage.start_link
    test_data = [
      {"key1", :some_data_1, :tag_1},
      {"key2", :some_data_2, :tag_1},
    ]

    :ok = DStorage.bulk_store(test_data)

    {:ok, results} = DStorage.bulk_retrieve_and_forget(:tag_1)
    assert Enum.count(results) == Enum.count(test_data)
    for t <- results do
      assert Enum.member?(test_data, t)
    end

    {:error, :enotag} = DStorage.bulk_retrieve_and_forget(:tag_1)

    DStorage.stop
  end

  test "bulk retrieve max count" do
    DStorage.start_link

    test_data = [
      {"key1", :some_data_1, :tag1},
      {"key2", :some_data_2, :tag1},
      {"key3", :some_data_3, :tag1}
    ]
    DStorage.bulk_store(test_data)

    max_count = 2

    {:ok, results} = DStorage.bulk_retrieve_and_forget(:tag1, max_count)
    assert Enum.count(results) == max_count

    for t <- results do
      assert Enum.member?(test_data, t)
    end

    DStorage.stop
  end
end
