defmodule BrokerTest do
  use ExUnit.Case, async: false
  doctest OctopusProject.Broker
  alias OctopusProject.{Broker,DStorage,TaskGenerator,Tree,Queue,Broker.WorkersSupervisor}
  require Logger

  @task_generator_opts %{
    workers_count:      1,
    dimensionality_d1:  3,
    dimensions:         :single,
    task_devision_rule: :equal,
    overlapping_rule:   :none,
    composition_rule:   :none,
    bucket_rule:        :batch
  }

  @overlap_task_generator_opts %{
    workers_count:      2,
    dimensionality_d1:  5,
    dimensions:         :single,
    task_devision_rule: :equal,
    overlapping_rule:   :overlap,
    overlapping_amount: 1,
    composition_rule:   :none,
    bucket_rule:        :batch
  }

  @broker_opts %{
    calc_fun: &BrokerTest.run_calc_job/4,
    conj_fun: &BrokerTest.run_conj_job/4,
    workers_count: 1
  }

  @to_fail_broker_opts %{
    conj_fun: &BrokerTest.run_fail_conj_job/4,
    workers_count: 1,
  }

  @doc """
  Example calc job
  """
  def run_calc_job(_, master_pid, [bucket_key], _) do
    # TODO NIF/Port call ?
    ten_times = fn(x) -> x * 10 end
    :rand.uniform |> trunc |> ten_times.() |> Process.sleep
    send(master_pid, {:done, {"ready_data_1", [1, 2, 3]}, [bucket_key]})
  end

  @doc """
  Example conj job
  """
  def run_conj_job(task_data, master_pid, bucket_names, _) do
    # ugly but without conjugator
    {lb, rb} = task_data[:conj_task_data]
    conj_tree = task_data[:conj_tree]
    new_bucket_name = Enum.join(bucket_names, "_")
    new_conj_tree = Tree.new(nil, conj_tree.value, conj_tree.right)
    [lb_name, rb_name] = bucket_names
    {:bucket, {_, lb_data}} = lb
    {:bucket, {_, rb_data}} = rb
    rb_keys = Enum.map(rb_data, fn {k, _} -> k end)
    shared_keys = Enum.filter_map(lb_data, fn {k, _} -> k in rb_keys end, fn {k, _} -> k end)
    shared_data = Enum.reduce(shared_keys, [], fn i, acc ->
      {_, lv} = Enum.find(lb_data, fn {k, _} -> k == i end)
      {_, rv} = Enum.find(rb_data, fn {k, _} -> k == i end)
      acc ++ [{i, "#{lv}#{rv}"}]
    end)
    res =
      Enum.filter(lb_data, fn {k, _} -> not k in shared_keys end) ++
      shared_data ++
      Enum.filter(rb_data, fn {k, _} -> not k in shared_keys end)
    new_bucket = [{:bucket, {new_bucket_name, res}}]
    conjugation_result = %{
      status: :merged,
      conj_tree: new_conj_tree,
      buckets: new_bucket,
      conj_task: {:conj_task, "bucket_666", "bucket_999"}
    }
    send(master_pid, {:conj_done, conjugation_result, bucket_names})
  end

  def run_fail_conj_job(task_data, master_pid, bucket_names, _) do
    {lb, rb} = task_data[:conj_task_data]
    [lb_name, rb_name] = bucket_names
    {:bucket, {_, lb_data}} = lb
    {:bucket, {_, rb_data}} = rb
    lb_data_new = for {k, _v} <- lb_data, do: {k, "NEW_VAL0"}
    rb_data_new = for {k, _v} <- rb_data, do: {k, "NEW_VAL1"}
    lb_new = {:bucket, {lb_name, lb_data_new}}
    rb_new = {:bucket, {rb_name, rb_data_new}}
    conjugation_result = %{
      status: :unmerged,
      conj_tree: task_data[:conj_tree],
      buckets: [lb_new, rb_new],
      conj_task: {:conj_task, lb_name, rb_name}
    }
    send(master_pid, {:conj_done, conjugation_result, bucket_names})
  end

  test "acquires computation job, one is executed and stored in d-storage" do
    DStorage.start_link
    TaskGenerator.start_link(@task_generator_opts)
    Broker.start_link(@broker_opts)
    WorkersSupervisor.start_link([[]])

    TaskGenerator.init_buckets
    DStorage.bulk_store([
      {"line_0", [1, 2, 3], :data},
      {"line_1", [4, 5, 6], :data},
      {"line_2", [7, 8, 9], :data},
      {"bucket_0", "", :task_comp_generated}
    ])
    Broker.acquire_task({:comp_task, "bucket_0"})
    Process.sleep(1000)
    assert {:ok, %{"bucket_0" => [1, 2, 3]}} == DStorage.retrieve("ready_data_1", :comp_results)

    # WorkersSupervisor.stop
    Broker.stop
    TaskGenerator.stop
    DStorage.stop
  end

  test "acquires conjugation job, one is executed (merged) and stored in d-storage" do
    DStorage.start_link
    TaskGenerator.start_link(@overlap_task_generator_opts)
    Broker.start_link(@broker_opts)
    WorkersSupervisor.start_link([[]])

    TaskGenerator.init_buckets
    DStorage.bulk_store([
      {"line_0", %{"bucket_0" => "a"}, :comp_results},
      {"line_1", %{"bucket_0" => "b"}, :comp_results},
      {"line_2", %{"bucket_0" => "c", "bucket_1" => "d"}, :comp_results},
      {"line_3", %{"bucket_1" => "e"}, :comp_results},
      {"line_4", %{"bucket_1" => "f"}, :comp_results}
    ])
    Broker.acquire_task({:conj_task, "bucket_0", "bucket_1"})
    Process.sleep(1000)
    {:ok, to_check_data} = DStorage.bulk_retrieve(["line_0", "line_1", "line_2", "line_3", "line_4"], :comp_results)
    to_check = to_check_data |> Enum.map(fn {k, v, _tag} -> {k, v["bucket_0_bucket_1"]} end)

    assert to_check == [
      {"line_0", "a"},
      {"line_1", "b"},
      {"line_2", "cd"},
      {"line_3", "e"},
      {"line_4", "f"}
    ]
    assert TaskGenerator.check_third_party_conj_tasks() == [{:conj_task, "bucket_666", "bucket_999"}]
    assert TaskGenerator.get_conj_tree().left == nil

    Broker.stop
    TaskGenerator.stop
    DStorage.stop
  end

  test "acquires conjugation job, one is executed (unmerged) and stored in d-storage" do
    DStorage.start_link
    TaskGenerator.start_link(@overlap_task_generator_opts)
    Broker.start_link(@to_fail_broker_opts)
    Queue.start_link
    WorkersSupervisor.start_link([[]])

    TaskGenerator.init_buckets
    initial_conj_tree = TaskGenerator.get_conj_tree()
    DStorage.bulk_store([
      {"line_0", %{"bucket_0" => "a"}, :comp_results},
      {"line_1", %{"bucket_0" => "b"}, :comp_results},
      {"line_2", %{"bucket_0" => "c", "bucket_1" => "d"}, :comp_results},
      {"line_3", %{"bucket_1" => "e"}, :comp_results},
      {"line_4", %{"bucket_1" => "f"}, :comp_results}
    ])
    Broker.acquire_task({:conj_task, "bucket_0", "bucket_1"})
    Process.sleep(1000)
    {:ok, to_check_data} = DStorage.bulk_retrieve(["line_0", "line_1", "line_2", "line_3", "line_4"], :comp_results)

    to_check_l = to_check_data
    |> Stream.map(fn {k, v, _tag} -> {k, v["bucket_0"]} end)
    |> Stream.filter(fn {k, v} -> v end)
    |> Enum.all?(fn {_k, v} -> v == "NEW_VAL0" end)

    to_check_r = to_check_data
    |> Stream.map(fn {k, v, _tag} -> {k, v["bucket_1"]} end)
    |> Stream.filter(fn {k, v} -> v end)
    |> Enum.all?(fn {_k, v} -> v == "NEW_VAL1" end)

    assert to_check_l
    assert to_check_r
    assert TaskGenerator.check_third_party_conj_tasks() == [{:conj_task, "bucket_0", "bucket_1"}]
    assert TaskGenerator.get_conj_tree() == initial_conj_tree

    Queue.stop
    Broker.stop
    TaskGenerator.stop
    DStorage.stop
  end

  test "acquires recomputation job, one is executed and stored to d-storage" do
    DStorage.start_link
    TaskGenerator.start_link(@task_generator_opts)
    Broker.start_link(@broker_opts)
    WorkersSupervisor.start_link([[]])

    TaskGenerator.init_buckets
    DStorage.bulk_store([
      {"line_0", %{"bucket_0" => [1, 2, 3]}, :comp_results},
      {"line_1", %{"bucket_0" => [4, 5, 6]}, :comp_results},
      {"line_2", %{"bucket_0" => [7, 8, 9]}, :comp_results},
      {"bucket_0", "", :task_comp_generated}
    ])
    Broker.acquire_task({:recomp_task, "bucket_0"})
    Process.sleep(1000)
    assert {:ok, %{"bucket_0" => [1, 2, 3]}} == DStorage.retrieve("ready_data_1", :comp_results)

    Broker.stop
    TaskGenerator.stop
    DStorage.stop
  end
end
