defmodule ConsumerTest do
  use ExUnit.Case, async: true
  doctest OctopusProject.Consumer
  alias OctopusProject.{Queue,Consumer}

  test "consuming messages" do
    args = %{
      dequeue_callback: fn({x, pid}) -> send(pid, x * 2) end
    }

    Queue.start_link
    Consumer.start_link(args)
    for x <- 1..3, do: Queue.enqueue({x, self()})
    res = for _ <- 1..3 do
      receive do
        x -> x
      after 500 ->
        nil
      end
    end
    assert res == [2, 4, 6]

    Consumer.stop
    Queue.stop
  end

end
