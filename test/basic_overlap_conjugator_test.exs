defmodule BasicOverlapConjugatorTest do
  use ExUnit.Case, async: false
  alias OctopusProject.Conjugator.BasicOverlapConjugator
  import BasicOverlapConjugator
  doctest BasicOverlapConjugator

  setup do
    lines = [
      {"line_1", [1.0, 1.0, 1.0]},
      {"line_2", [2.0, 2.0, 2.0]},
      {"line_3", [3.0, 3.0, 3.0]},
      {"line_4", [4.0, 4.0, 4.0]}
    ]

    lines_distant = [
      {"line_1", [1.5, 1.5, 1.5]},
      {"line_2", [2.5, 2.5, 2.5]},
      {"line_3", [3.5, 3.5, 3.5]},
      {"line_4", [4.5, 4.5, 4.5]}
    ]

    bucket_1 = {:bucket, {"b1", Enum.take(lines, 3)}} # lines 1, 2, 3
    bucket_2 = {:bucket, {"b2", Enum.drop(lines, 1)}} # lines 2, 3, 4
    bucket_3 = {:bucket, {"b3", Enum.drop(lines_distant, 1)}} # same but far away

    {:ok,
     [
       b_1: bucket_1,
       b_2: bucket_2,
       b_3: bucket_3,
       shared_keys: ["line_2", "line_3"],
       orig_lines: lines
     ]
    }
  end

  test "exit_merge_reason/3 for relatively close buckets", context do
    assert exit_merge_reason?(context[:b_1], context[:b_2], context[:shared_keys]) == true
  end

  test "exit_merge_reason/3 for relatively distant buckets", context do
    assert exit_merge_reason?(context[:b_2], context[:b_3], context[:shared_keys]) == false
  end

  test "mutate_buckets/3 for relatively close buckets", context do
    # test corresponding values contraction
    #
    # relatively close buckets contract to themselves

    {{:bucket_1, b1_mut}, {:bucket_2, b2_mut}} =
      mutate_buckets(context[:b_1], context[:b_2], context[:shared_keys])

    {:bucket, {"b1", b1_data}} = b1_mut
    {:bucket, {"b2", b2_data}} = b2_mut

    # keys for lines still the same
    belongs_to_keys_set_fun = fn {k, _} -> k in ["line_1", "line_2", "line_3", "line_4"] end
    assert(Enum.all?(b1_data, belongs_to_keys_set_fun) and Enum.count(b1_data) == 3)
    assert(Enum.all?(b2_data, belongs_to_keys_set_fun) and Enum.count(b2_data) == 3)

    # assert contraction
    shared_data_b1 = Enum.drop(b1_data, 1)
    shared_data_b2 = Enum.take(b2_data, 2)

    assert(
      Stream.zip(shared_data_b1, shared_data_b2)
      |> Stream.map(fn {{_l_k1, l_b1}, {_l_k2, l_b2}} ->
        Stream.zip(l_b1, l_b2) |> Enum.all?(fn {l, r} -> abs(l - r) < 0.0001 end)
      end)
      |> Enum.all?
    )
  end

  test "mutate_buckets/3 for relatively distant buckets", context do
    # test corresponding values contraction
    #
    # relatively distant buckets contract to their mean

    {{:bucket_1, b1_mut}, {:bucket_2, b3_mut}} =
      mutate_buckets(context[:b_1], context[:b_3], context[:shared_keys])

    {:bucket, {"b1", b1_data}} = b1_mut
    {:bucket, {"b3", b3_data}} = b3_mut

    # keys for lines still the same
    belongs_to_keys_set_fun = fn {k, _} -> k in ["line_1", "line_2", "line_3", "line_4"] end
    assert(Enum.all?(b1_data, belongs_to_keys_set_fun) and Enum.count(b1_data) == 3)
    assert(Enum.all?(b3_data, belongs_to_keys_set_fun) and Enum.count(b3_data) == 3)

    # assert inequality
    assert(b1_mut != context[:b1])
    assert(b3_mut != context[:b3])

    # assert contraction
    shared_data_bl = Enum.drop(b1_data, 1)
    shared_data_br = Enum.take(b3_data, 2)

    assert(
      Stream.zip(shared_data_bl, shared_data_br)
      |> Stream.map(fn {{_l_k1, l_b1}, {_l_k2, l_b2}} ->
        Stream.zip(l_b1, l_b2) |> Enum.all?(fn {l, r} -> abs(l - r) < 0.0001 end)
      end)
      |> Enum.all?
    )
  end

  test "merge/3 for relatively close buckets", context do
    {:bucket, {"[b1+b2]", b1_b2_data}} =
      merge(context[:b_1], context[:b_2], "[b1+b2]", context[:shared_keys])

    belongs_to_keys_set_fun = fn {k, _} -> k in ["line_1", "line_2", "line_3", "line_4"] end
    assert(Enum.all?(b1_b2_data, belongs_to_keys_set_fun) and Enum.count(b1_b2_data) == 4)

    assert b1_b2_data == context[:orig_lines]
  end

end
