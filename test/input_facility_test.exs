defmodule InputFacilityTest do
  use ExUnit.Case, async: false
  alias OctopusProject.{InputFacility,DStorage}
  doctest InputFacility

  @filename "test/ifac_test_file.in"

  test "convert string to list of floats" do
    DStorage.start_link
    origin = "1.0 2.0 3.0 0.0"
    origin_processed = [1.0, 2.0, 3.0, 0.0]
    assert InputFacility.str_to_list_f(origin) == origin_processed
    DStorage.stop
  end

  test "process file input and fill db" do
    DStorage.start_link
    lines = @filename
    |> File.read!
    |> String.splitter("\n", [trim: true])
    |> Enum.into([])

    InputFacility.start_link(%{filename: @filename, d1_prefix: "line_"}, :file, self())

    receive do
      :ifac_file_processed ->
        :ok
    after 1500 ->
      throw :ifac_no_respond
    end

    {:ok, payload} = DStorage.lookup(:data)
    assert Enum.count(lines) == Enum.count(payload)
    for {l, data_l} <- List.zip([lines, payload]) do
      {_, data, _} = data_l
      assert InputFacility.str_to_list_f(l) == data
    end
    DStorage.stop
  end
end
