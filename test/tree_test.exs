defmodule TreeTest do
  alias OctopusProject.Tree
  use ExUnit.Case
  doctest Tree

  test "new" do
    mock = Tree.new(Tree.new(nil, 1, nil), 2, Tree.new(nil, 3, nil))

    assert mock.value == 2
    assert mock.left.value == 1
    assert mock.left.left == nil
    assert mock.left == Tree.new(nil, 1, nil)
    assert mock.right.value == 3
    assert mock.right.right == nil
    assert mock.right == Tree.new(nil, 3, nil)
  end

  test "find" do
    mock = Tree.new(
      Tree.new(nil,1,Tree.new(nil,2,nil)),
      0,
      Tree.new(Tree.new(nil,4,nil),3,nil)
    )

    assert Tree.find(mock, 0) == mock
    assert Tree.find(mock, 4) == Tree.new(nil,4,nil)
    assert Tree.find(mock, 1) ==  Tree.new(nil,1,Tree.new(nil,2,nil))
    assert Tree.find(mock, 11) == nil
  end

  test "find_parent" do
    mock = Tree.new(
      Tree.new(nil,1,Tree.new(nil,2,nil)),
      0,
      Tree.new(Tree.new(nil,4,nil),3,nil)
    )

    assert Tree.find_parent(mock, Tree.new(nil,2,nil)) == Tree.new(nil,1,Tree.new(nil,2,nil))
    assert Tree.find_parent(mock, mock) == nil
    assert Tree.find_parent(mock, Tree.new(nil, 11, nil)) == nil
    assert Tree.find_parent(mock, Tree.new(Tree.new(nil,4,nil),3,nil)) == mock
  end

  test "update_node" do
    mock = Tree.new(
      Tree.new(nil,1,Tree.new(nil,2,nil)),
      0,
      Tree.new(Tree.new(nil,4,nil),3,nil)
    )

    assert Tree.update_node(mock, mock.left, {nil, nil, nil}) ==
      Tree.new(
        Tree.new(nil,nil,nil),
        0,
        Tree.new(Tree.new(nil,4,nil),3,nil)
      )

    assert Tree.update_node(mock, Tree.new(nil,4,nil), {nil, 5, nil}) ==
      Tree.new(
        Tree.new(nil,1,Tree.new(nil,2,nil)),
        0,
        Tree.new(Tree.new(nil,5,nil),3,nil)
      )

    assert Tree.update_node(mock, Tree.new(Tree.new(nil,4,nil),3,nil), {nil,1,Tree.new(nil,2,nil)}) ==
      Tree.new(
        Tree.new(nil,1,Tree.new(nil,2,nil)),
        0,
        Tree.new(nil,1,Tree.new(nil,2,nil))
      )
  end

  test "same_parent_by_v?" do
    mock = Tree.new(
      Tree.new(nil,1,Tree.new(nil,2,nil)),
      0,
      Tree.new(Tree.new(nil,4,nil),3,nil)
    )

    assert Tree.same_parent_by_v?(mock, 1, 3) == true
    assert Tree.same_parent_by_v?(mock, 3, 1) == true
    assert Tree.same_parent_by_v?(mock, 3, 4) == false
  end

  test "reverse_grow_tree" do
    mock = [1, 2, 3, 4]
    assert Tree.reverse_grow_tree(mock) ==
      Tree.new(
        Tree.new(Tree.new(nil, 1, nil), nil, Tree.new(nil, 2, nil)),
        nil,
        Tree.new(Tree.new(nil, 3, nil), nil, Tree.new(nil, 4, nil))
      )

    mock2 = [1, 2, 3]
    assert Tree.reverse_grow_tree(mock2) ==
      Tree.new(
        Tree.new(Tree.new(nil, 1, nil), nil, Tree.new(nil, 2, nil)),
        nil,
        Tree.new(nil, 3, nil)
      )
  end
end
