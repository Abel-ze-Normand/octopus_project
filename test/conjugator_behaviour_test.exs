defmodule ConjugatorBehaviourTest do
  alias OctopusProject.{Conjugator, DStorage, Tree}
  use ExUnit.Case, async: false
  doctest Conjugator

  setup do
    context = %{
      samples: %{
        "key_0" => [1, 1, 1],
        "key_1" => [2, 2, 2],
        "key_2" => [3, 3, 3],
        "key_3" => [4, 4, 4],
        "key_4" => [5, 5, 5]
      },
      conj_tree: Tree.new(
        Tree.new(
          Tree.new(nil, "bucket_1", nil),
          nil,
          Tree.new(nil, "bucket_2", nil)
        ),
        nil,
        Tree.new(
          Tree.new(nil, "bucket_3", nil),
          nil,
          Tree.new(nil, "bucket_4", nil)
        )
      ),
      bucket_1: {:bucket, {"bucket_1", ["key_0", "key_1"]}},
      bucket_2: {:bucket, {"bucket_2", ["key_1", "key_2"]}},
      bucket_3: {:bucket, {"bucket_3", ["key_2", "key_3"]}},
      bucket_4: {:bucket, {"bucket_4", ["key_3", "key_4"]}}
    }
    {:ok, context}
  end

  test "reduction, final reduction", context do

    defmodule MockNoOverwriteConjugatorTest do
      use Conjugator

      @bucket_1 {:bucket, {"bucket_1", ["key_0", "key_1"]}}
      @bucket_2 {:bucket, {"bucket_2", ["key_1", "key_2"]}}

      def merge(bucket_1, bucket_2, merged_name, shared_keys) do
        assert bucket_1 == @bucket_1
        assert bucket_2 == @bucket_2
        assert merged_name == "[bucket_1+bucket_2]"
        assert shared_keys == ["key_1"]
        {:bucket, {merged_name, ["key_0", "key_1", "key_2"]}}
      end

      def exit_merge_reason?(_bucket_1, _bucket_2, _shared_keys) do
        :ok
      end

      def mutate_buckets(bucket_1, bucket_2, _shared_keys) do
        {{:bucket_1, bucket_1}, {:bucket_2, bucket_2}}
      end
    end

    to_check_tree = Tree.new(
      Tree.new(nil, "[bucket_1+bucket_2]", nil),
      nil,
      context[:conj_tree].right
    )

    conj_dec = MockNoOverwriteConjugatorTest.conjugate(context[:bucket_1], context[:bucket_2], context[:conj_tree])

    assert conj_dec[:status] == :merged
    assert conj_dec[:buckets] == [{:bucket, {"[bucket_1+bucket_2]", ["key_0", "key_1", "key_2"]}}]
    assert conj_dec[:conj_task] == {:final, {:conj_task, "[bucket_1+bucket_2]", "[bucket_3+bucket_4]"}}
    assert conj_dec[:conj_tree] == to_check_tree
  end

  test "repetitive reductions", context do

    defmodule MockRepetitiveConjugatorTest do
      use Conjugator

      def merge(bucket_1, bucket_2, merged_name, shared_keys) do
        {:bucket, {bucket_1_name, keys_1}} = bucket_1
        {:bucket, {bucket_2_name, keys_2}} = bucket_2
        assert merged_name == "[#{bucket_1_name}+#{bucket_2_name}]"
        {:bucket, {merged_name, (keys_1 -- shared_keys) ++ keys_2}}
      end

      def exit_merge_reason?(_bucket_1, _bucket_2, _shared_keys) do
        :ok
      end

      def mutate_buckets(bucket_1, bucket_2, _shared_keys) do
        {{:bucket_1, bucket_1}, {:bucket_2, bucket_2}}
      end
    end

    to_check_tree_1 = Tree.new(
      Tree.new(nil, "[bucket_1+bucket_2]", nil),
      nil,
      context[:conj_tree].right
    )
    to_check_tree_2 = Tree.new(
      Tree.new(nil, "[bucket_1+bucket_2]", nil),
      nil,
      Tree.new(nil, "[bucket_3+bucket_4]", nil)
    )

    conj_dec = MockRepetitiveConjugatorTest.conjugate(context[:bucket_1], context[:bucket_2], context[:conj_tree])

    assert conj_dec[:status] == :merged
    assert conj_dec[:buckets] == [{:bucket, {"[bucket_1+bucket_2]", ["key_0", "key_1", "key_2"]}}]

    {:final, {:conj_task, b1, b2}} = conj_dec[:conj_task]

    assert b1 in ["[bucket_1+bucket_2]", "[bucket_3+bucket_4]"]
    assert b2 in ["[bucket_1+bucket_2]", "[bucket_3+bucket_4]"]
    assert b1 != b2
    assert conj_dec[:conj_tree] == to_check_tree_1

    new_conj_tree = conj_dec[:conj_tree]

    conj_dec = MockRepetitiveConjugatorTest.conjugate(context[:bucket_3], context[:bucket_4], new_conj_tree)

    assert conj_dec[:status] == :merged
    assert conj_dec[:buckets] == [{:bucket, {"[bucket_3+bucket_4]", ["key_2", "key_3", "key_4"]}}]

    {:final, {:conj_task, b1, b2}} = conj_dec[:conj_task]

    assert b1 in ["[bucket_1+bucket_2]", "[bucket_3+bucket_4]"]
    assert b2 in ["[bucket_1+bucket_2]", "[bucket_3+bucket_4]"]
    assert b1 != b2
    assert conj_dec[:conj_tree] == to_check_tree_2
  end

  test "test mutation", context do
    defmodule MockMutationConjugatorTest do
      use Conjugator

      def merge(_bucket_1, _bucket_2, merged_name, _shared_keys) do
        # never reach this func
        assert false
        {:bucket, {merged_name, ["key_0", "key_1", "key_2"]}}
      end

      def exit_merge_reason?(_bucket_1, _bucket_2, _shared_keys) do
        :not_enough
      end

      # mutation is side-effected
      def mutate_buckets(bucket_1, bucket_2, shared_keys) do
        {:bucket, {_bucket_1_name, _keys_1}} = bucket_1
        {:bucket, {_bucket_2_name, _keys_2}} = bucket_2

        data = for k <- shared_keys, do: elem(DStorage.retrieve(k, :data), 1)

        # mutation
        for k <- shared_keys do
          for l <- data do
            d = for _ <- l, do: 10
            DStorage.store(k, d, :data)
          end
        end

        {{:bucket_1, bucket_1}, {:bucket_2, bucket_2}}
      end
    end

    DStorage.start_link
    for {k, v} <- context[:samples], do: DStorage.store(k, v, :data)

    conj_dec = MockMutationConjugatorTest.conjugate(context[:bucket_1], context[:bucket_2], context[:conj_tree])
    # shared data = key_1
    assert DStorage.retrieve("key_1", :data) == {:ok, Enum.map(context[:samples]["key_1"], fn _ -> 10 end)}
    assert conj_dec[:status] == :unmerged
    assert conj_dec[:buckets] == [context[:bucket_1], context[:bucket_2]]
    assert conj_dec[:conj_task] == {:conj_task, "bucket_1", "bucket_2"}
    assert conj_dec[:conj_tree] == context[:conj_tree]

    DStorage.stop
  end

  test "natural sorting feature" do
    defmodule EmptyConjugatorTest do
      use Conjugator
      def merge(_, _, _, _), do: nil
      def exit_merge_reason?(_, _, _), do: :false
      def mutate_buckets(_, _, _), do: nil
    end

    lines = [
      {"line_1", 1},
      {"line_11", 11},
      {"line_0", 0},
      {"line_56", 56},
      {"line_2", 2},
      {"line_12", 12},
      {"line_9", 9},
      {"line_10", 10}
    ]

    correct_lines_order = [
      {"line_0", 0},
      {"line_1", 1},
      {"line_2", 2},
      {"line_9", 9},
      {"line_10", 10},
      {"line_11", 11},
      {"line_12", 12},
      {"line_56", 56}
    ]
    test_bucket = {:bucket, {"bucket_name", lines}}
    {:bucket, {"bucket_name", res_lines}} = EmptyConjugatorTest.natural_sort_bucket_keys(test_bucket)
    assert res_lines == correct_lines_order
  end
end
