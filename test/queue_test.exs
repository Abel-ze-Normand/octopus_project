defmodule QueueTest do
  use ExUnit.Case, async: false
  doctest OctopusProject.Queue
  alias OctopusProject.Queue, as: Queue

  test "start/stop queue server" do
    Queue.start_link
    Queue.stop
  end

  test "enqueue message to empty queue" do
    Queue.start_link
    assert {:ok, _} = Queue.enqueue(:some_msg)
    Queue.stop
  end

  test "dequeue message from empty queue" do
    Queue.start_link
    assert {:error, :eemptyq} == Queue.dequeue
    Queue.stop
  end

  test "enqueue message and dequeue it from empty queue" do
    Queue.start_link
    assert {:ok, _} = Queue.enqueue(:some_msg)
    assert :some_msg = Queue.dequeue
    Queue.stop
  end

  test "enqueue 3 messages and dequeue them in proper order" do
    Queue.start_link
    {:ok, _} = Queue.enqueue(:msg1)
    {:ok, _} = Queue.enqueue(:msg2)
    {:ok, _} = Queue.enqueue(:msg3)
    assert :msg1 == Queue.dequeue
    assert :msg2 == Queue.dequeue
    assert :msg3 == Queue.dequeue
    assert {:error, :eemptyq} == Queue.dequeue
    Queue.stop
  end

  test "default (1000) threshold messages in queue" do
    Queue.start_link
    Enum.each 0..999, fn(x) ->
      Queue.enqueue(x)
    end
    assert {:error, :efullq} == Queue.enqueue(:msg1)
    Queue.stop
  end

  test "customizeble threshold messages in queue and Queue#enqueue returns available space" do
    threshold = 5
    Queue.start_link(threshold: threshold)
    Enum.each 0..4, fn(x) ->
      assert {:ok, threshold - 1 - x } == Queue.enqueue(:msg)
    end
    assert {:error, :efullq} == Queue.enqueue(:msg1)
    Queue.stop
  end

  test "when full queue, enqueue return error, then dequeue and successfull enqueue" do
    threshold = 2
    Queue.start_link(threshold: threshold)
    {:ok, _} = Queue.enqueue(:msg)
    {:ok, _} = Queue.enqueue(:msg)
    assert {:error, :efullq} == Queue.enqueue(:msg1)
    assert :msg == Queue.dequeue
    assert {:ok, 0} == Queue.enqueue(:msg1)
    Queue.stop
  end


  test "concurrent enqueue messages" do
    threshold = 5
    Queue.start_link(threshold: threshold)
    Enum.each 1..2, fn(proc_num) ->
      spawn_link fn ->
        Enum.each 0..2, fn(i) -> Queue.enqueue({proc_num, i}) end
      end
    end
    contents = for _x <- 0..4, do: Queue.dequeue
    # if concurrent inserts worked properly, then there are no duplications
    assert Enum.count(Enum.uniq(contents)) == Enum.count(contents)
    Queue.stop
  end

  test "concurrent dequeue messages" do
    threshold = 5
    Queue.start_link(threshold: threshold)
    desirable_result = [1, 2, 3, 4, 5]
    for x <- desirable_result, do: Queue.enqueue(x)
    for _x <- 0..4 do
      pid = self()
      spawn_link fn ->
        v = Queue.dequeue
        send(pid, v)
      end
    end

    res_list = for _x <- 0..4 do
      receive do
        v -> v
      after
        5 -> :noval
      end
    end

    assert desirable_result == Enum.sort(res_list)
    Queue.stop
  end

  defmodule TestConsumer do
    alias OctopusProject.Queue, as: Queue
    def consume(pid) do
      spawn fn ->
        case Queue.dequeue do
          {:error, :eemptyq} ->
            consume(pid)
          item               ->
            send(pid, item)
            consume(pid)
        end
      end
    end
  end

  test "subscription mechanism" do
    Queue.start_link
    desirable_result = [1, 2, 3, 4, 5]
    for x <- desirable_result, do: Queue.enqueue(x)
    TestConsumer.consume(self())
    res_list = for _x <- desirable_result do
      receive do
        item -> item
      after
        1_000 -> :noval
      end
    end
    assert desirable_result == Enum.sort(res_list)
    Queue.stop
  end
end
