# Octopus Project <code>[4/8]</code><a id="sec-3" name="sec-3"></a>

- [x] core (SYPHON part)<a id="sec-3-1" name="sec-3-1"></a>

- [ ] distributed run<a id="sec-3-2" name="sec-3-2"></a>

- [ ] output facility<a id="sec-3-3" name="sec-3-3"></a>

- [x] supervision tree<a id="sec-3-4" name="sec-3-4"></a>

- [x] compose everything<a id="sec-3-5" name="sec-3-5"></a>
 
- [x] test run<a id="sec-3-6" name="sec-3-6"></a>
 
- [ ] Configurator (epic)<a id="sec-3-7" name="sec-3-7"></a>

- [ ] composition rule

# Deployment

requires elixir 1.4.0 and nightly rust (CI still broken)